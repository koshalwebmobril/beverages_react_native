import React, {Component} from 'react';
import messaging from '@react-native-firebase/messaging';
import firebase from '@react-native-firebase/app';

import Navigator from './src/services/Navigator';
import NavigationService from './src/services/navigationService';
import PushNotificationIOS from "@react-native-community/push-notification-ios";
var PushNotification = require("react-native-push-notification");

// Register background handler
messaging ().setBackgroundMessageHandler (async remoteMessage => {
  console.log ('Message handled in the background!', remoteMessage);
 // NavigationService.navigate ('Notifications');
 showLocalPushNotification(remoteMessage);
});

messaging ().getInitialNotification ().then (remoteMessage => {
  if (remoteMessage) {
    NavigationService.navigate ('Notifications');
  }
});

messaging ().onNotificationOpenedApp (remoteMessage => {
  console.log (
    'Notification caused app to open from background state:',
    remoteMessage.notification
  );
  NavigationService.navigate ('Notifications');
});
// When a user receives a push notification and the app is in foregroun
 messaging ().onMessage (async remoteMessage => {
  
     showLocalPushNotification(remoteMessage);

   });

 const showLocalPushNotification = remoteMessage => {
  //const { notification = {}, data = {} } = remoteMessage; 
  // showLocalPushNotification(remoteMessage);
  // console.log (
  //   'Message handled in the forground!',
  //   JSON.stringify (remoteMessage)  
  //            );
  // PushNotification.localNotification({
  //   //smallIcon: 'ic_notification', 
  //   // todo uncomment when resource is added
  //   channelId: '', // todo
  //   messageId: 'google:message_id',
  //   title:' notification.title',
  //   message: 'notification.body',
  //   sound: 'default',
  //   data:{}
  // });
};
 
 
class App extends Component {
  constructor (props) {
    super (props);
    this.state = {};
  }
  
  render () {
    return (
      <Navigator
        ref={navigatorRef => {
          NavigationService.setTopLevelNavigator (navigatorRef);
        }}
      />
    );
  }
}

export default App;
