/**
 * @format
 */

import {AppRegistry} from 'react-native';
import React from 'react';
import App from './App';
import {name as appName} from './app.json';
import {Provider} from 'react-redux';
import {store, persistor} from './src/services/redux/store/configureStore';
import {PersistGate} from 'redux-persist/integration/react';

//import PushNotificationIOS from "@react-native-community/push-notification-ios";
//var PushNotification = require("react-native-push-notification");
// PushNotification.configure({
//   onRegister:  (token) =>{
//     console.log(token)
// },
//   onNotification: (notification) => {
//    // console.log('NOTIFICATION:', JSON.stringify(notification));
//    PushNotification.localNotification({
//     //smallIcon: 'ic_notification', // todo uncomment when resource is added
//     channelId: '', // todo
//     messageId: 'google:message_id',
//     title: 'notification.title',
//     message: 'notification.body',
//     sound: 'default',
//     data:{}
//   });
// },
//   permissions: {
//       alert: true,
//       badge: true,
//       sound: true,
//   },
//   popInitialNotification: true,
//   requestPermissions: true,
// })

const RNRedux = () => (
  <Provider store={store}>
    <PersistGate persistor={persistor} loading={null}>
      <App />
    </PersistGate>
  </Provider>
);

AppRegistry.registerComponent(appName, () => RNRedux);

  /**
   * ANDROID 
   * keystore password ---- andorid
   * alias - key0
   * alias pass- password
   */
