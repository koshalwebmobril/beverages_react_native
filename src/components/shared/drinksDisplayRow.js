import React, {Component} from 'react';
import {
  View,
  Text,
  ScrollView,
  Dimensions,
  Image,
  Platform,
  BackHandler,
} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import STRINGS from '../../utils/strings';
import {fontSize} from '../../components/global/Fontsize';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {Divider} from 'react-native-elements';

const screenWidth = Dimensions.get ('window').width;
EStyleSheet.build ({$rem: screenWidth / 380});
const fontSizeValue = fontSize ();

class drinksDisplayRow extends Component {
  constructor (props) {
    super (props);
    this.state = {
      quantity: 0
    };
  }

  navigateToListing = () => {
    BackHandler.removeEventListener (
      'hardwareBackPress',
      this.props.handleBackButton
    );
    this.props.navigation.navigate ('ProductListing', {
      title: this.props.drinkTitle,
      searchList: this.props.data,
    });
  };

  render () {
    return (
      <View>
        <View style={styles.drinkTypeRow}>
          <TouchableOpacity onPress={this.navigateToListing}>
            <Text style={styles.drinktype}>{this.props.drinkTitle}</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={this.navigateToListing}>
            <Text style={[styles.viewAll,{textDecorationLine: Platform.OS=='ios'? 'underline':null,}]}>{STRINGS.home.viewAll}</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.drinksRow}>
          {this.props.data.slice (0, 3).map (item => {
            return (
              <View key={item.id} style={styles.rowLayout}>
                <TouchableOpacity onPress={() => {
                  BackHandler.removeEventListener (
                    'hardwareBackPress',
                    this.props.handleBackButton
                  );
                  this.props.navigation.navigate ('ProductDetails', {
                    title: item.product_title,
                    quantity: this.state.quantity,
                    item: item,
                    productId: item.id,
                  });
                }} style={styles.drinks}>
                  <View>
                    <Image
                      source={{uri: item.product_image}}
                      style={styles.imageStyle}
                    />
                  </View>
                  <View style={styles.nameRow}>
                    <Text style={styles.drinkNameStyle}>
                      {item.product_title}
                    </Text>
                  </View>
                </TouchableOpacity>
              </View>
            );
          })}
          <TouchableOpacity
            activeOpacity={0.7}
            style={[styles.fwdIconStyle,{top: -15 }]}
            onPress={this.navigateToListing}
          >
            <Image
              source={require ('../../assets/forward.png')}
              style={styles.forwardImageStyle}
            />
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

export default drinksDisplayRow;

const styles = EStyleSheet.create ({
  drinktype: {
    fontSize: fontSizeValue * 15,
    fontFamily: Platform.OS == 'android' ? 'Calisto-Bold' : 'Calisto MT',
    fontWeight:'bold',
    color: '#47170d',
    fontWeight: 'bold',
  },
  viewAll: {
    fontSize: fontSizeValue * 10,
    fontFamily: Platform.OS == 'android' ? 'Calisto-Bold' : 'Calisto MT',
    color: '#47170d',
    borderBottomWidth: 0.5,
    marginBottom: '5rem',
    fontWeight:'bold',
    
  },
  drinkTypeRow: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginRight: '15rem',
    marginLeft: '10rem',
    alignItems: 'flex-end',
    marginBottom: '10rem',
  },
  drinks: {
    flexDirection: 'column',
  },
  imageStyle: {
    height: '90rem',
    width: '90rem',
    borderRadius: '10rem',
  },
  drinkNameStyle: {
    fontSize: fontSizeValue * 9,
    fontFamily: Platform.OS == 'android' ? 'Calisto-Regular' : 'Calisto MT',
    color: '#47170d',
    marginTop: '8rem',
  },
  drinksRow: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'flex-start',
    marginLeft: '10rem',
    marginRight: '10rem',
  },
  forwardImageStyle: {
    height: '30rem',
    width: '30rem',
  },
  rowLayout: {
    flex: 0.2,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  fwdIconStyle: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    
  },
  nameRow: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
});
