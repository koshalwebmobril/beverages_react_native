import React, {Component} from 'react';
import {View, Text, ScrollView, Dimensions,Platform,BackHandler} from 'react-native';
import {connect} from 'react-redux';
import EStyleSheet from 'react-native-extended-stylesheet';
import STRINGS from '../../utils/strings';
import {fontSize} from '../../components/global/Fontsize';
import {TouchableOpacity} from 'react-native-gesture-handler';

const screenWidth = Dimensions.get('window').width;
EStyleSheet.build({$rem: screenWidth / 380});
const fontSizeValue = fontSize();

class filter extends Component {
  constructor(props) {
    super(props);
    this.state = {
      products: [],
      categories: [],
      selectedItem: null
    };
 }

  componentDidMount() {
    var productsData = this.props.productsReducer.products;
    if (productsData) {
      var products = productsData.products;
      var categories = productsData.categories;
      this.setState({products: products ? products :[], categories: categories});
    }
    this.onLoad()
  }

  onLoad = () => {
    this.didFocusListener = this.props.navigation.addListener ('didFocus', () => this.setState({selectedItem: null}));
  }

  componentWillUnmount () {
    // Remove the event listener
    this.didFocusListener.remove();
  }

  onSelectHome = () => {
    BackHandler.removeEventListener('hardwareBackPress', this.props.handleBackButton);
    this.setState ({
      selectedItem: this.props.item.id,
    });
    if (this.props.item.category_name == 'All') {
      this.props.navigation.navigate('ProductListing', {
        title: STRINGS.home.all,
        searchList: this.state.products,
        selectedItem: this.props.item.id
      });
    } else {
      let searchListHome = this.state.products.filter((product) => {
        return product.category_name == this.props.item.category_name;
      });
      this.props.navigation.navigate('ProductListing', {
        title: this.props.item.display_name,
        searchList: searchListHome,
        selectedItem: this.props.item.id
      });
    }
  };

  render() {
    const isSelected = this.state.selectedItem === this.props.item.id;
    return (
      <View>
        <View style={styles.filters}>
          <TouchableOpacity
            activeOpacity={0.7}
            style={isSelected == false ? styles.category : styles.categoryBorder}
            onPress={this.onSelectHome}>
            <Text style={styles.categoryStyle}>
              {this.props.item.category_name}
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  const {userReducer, productsReducer} = state;
  return {userReducer, productsReducer};
};
export default connect(mapStateToProps)(filter);

const styles = EStyleSheet.create({
  filters: {
    flexDirection: 'row',
    marginLeft: '8rem',
  },
  category: {
    paddingTop: '10rem',
    paddingBottom: '10rem',
    paddingLeft: '20rem',
    paddingRight: '20rem',
    elevation: 20,
    borderRadius: '8rem',
    borderRightWidth: '4rem',
    borderLeftWidth: '4rem',
    borderTopWidth: '1.5rem',
    borderBottomWidth: '1.5rem',
    borderColor:'rgba(128,128,128,0.5)',
    marginBottom: '10rem',
    marginTop: '10rem',
    marginRight: '10rem',
    backgroundColor: 'white',
  },
  categoryBorder: {
    paddingTop: '10rem',
    paddingBottom: '10rem',
    paddingLeft: '20rem',
    paddingRight: '20rem',
    elevation: 1,
    borderRadius: '8rem',
    marginBottom: '10rem',
    marginTop: '10rem',
    marginRight: '10rem',
    backgroundColor: 'rgba(18,176,7,0.1)'
 },
  categoryStyle: {
    color: 'black',
    fontSize: fontSizeValue * 10,
    fontFamily:  Platform.OS == 'android' ? 'Calisto-Regular' : 'Calisto MT',
  },
});
