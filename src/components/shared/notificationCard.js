import React, {Component} from 'react';
import {Dimensions, View, Text, Image,Platform} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import STRINGS from '../../utils/strings';
import {fontSize} from '../global/Fontsize';

const screenWidth = Dimensions.get('window').width;
EStyleSheet.build({$rem: screenWidth / 380});
const fontSizeValue = fontSize();

class notificationCard extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <View style={styles.cardMargin}>
        <View style={styles.cardBackground}>
          <View style={styles.iconColumn}>
            <Image
              source={
                this.props.screen == 'lastOrder'
                  ? require('../../assets/order.png')
                  : require('../../assets/redBell.png')
              }
              style={styles.accIconStyle}
            />
          </View>
          <View style={styles.textColumn}>
            <Text style={styles.notificationText}>
              {this.props.notificationText}
            </Text>
          </View>
        </View>
      </View>
    );
  }
}

export default notificationCard;

const styles = EStyleSheet.create({
  cardBackground: {
    flexDirection: 'row',
    backgroundColor: 'white',
    height: '70rem',
    marginLeft: '10rem',
    marginRight: '10rem',
    marginTop: '10rem',
    elevation: 10,
  },
  accIconStyle: {
    padding: '10rem',
    margin: '15rem',
    height: '50rem',
    width: '50rem',
    resizeMode: 'contain',
    alignItems: 'center',
    justifyContent: 'center',
  },
  iconColumn: {
    flexDirection: 'column',
  },
  textColumn: {
    flexDirection: 'column',
    width: '75%',
    justifyContent: 'center',
  },
  notificationText: {
    fontSize: fontSizeValue * 10,
    fontFamily:  Platform.OS == 'android' ? 'Calisto-Regular' : 'Calisto MT',
    color: 'black',
  },
  cardMargin: {
    marginBottom: '5rem',
  },
});
