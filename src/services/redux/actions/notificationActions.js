import {NOTIFICATION_COUNT} from '../constants/index';

export function updateNotificationCount(notificationCount) {
  return {
    type: NOTIFICATION_COUNT,
    payload: notificationCount,
  };
}
