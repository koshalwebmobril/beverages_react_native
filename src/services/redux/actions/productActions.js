import {PRODUCT_DETAILS} from '../constants/index';

export function productUpdateDetails(products) {
  return {
    type: PRODUCT_DETAILS,
    payload: products,
  };
}
