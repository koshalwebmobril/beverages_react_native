import {CART_COUNT} from '../constants/index';

const initialState = {
  cartCount: 0,
};

const cartReducer = (state = initialState, action) => {
  switch (action.type) {
    case CART_COUNT:
      return {
        ...state,
        cartCount: action.payload,
      };
    default:
      return state;
  }
};

export default cartReducer;
