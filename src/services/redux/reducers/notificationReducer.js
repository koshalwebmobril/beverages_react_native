import {NOTIFICATION_COUNT} from '../constants/index';

const initialState = {
  notificationCount: 0,
};

const notificationReducer = (state = initialState, action) => {
  switch (action.type) {
    case NOTIFICATION_COUNT:
      return {
        ...state,
        notificationCount: action.payload,
      };
    default:
      return state;
  }
};

export default notificationReducer;
