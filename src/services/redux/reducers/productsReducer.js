import {PRODUCT_DETAILS} from '../constants/index';

const initialState = {
  products: {},
};

const productsReducer = (state = initialState, action) => {
  switch (action.type) {
    case PRODUCT_DETAILS:
      return {
        ...state,
        products: action.payload,
      };
    default:
      return state;
  }
};

export default productsReducer;
