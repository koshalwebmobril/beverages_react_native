import EStyleSheet from 'react-native-extended-stylesheet';
import {fontSize} from '../components/global/Fontsize';
import {Dimensions,Platform} from 'react-native';

const entireScreenWidth = Dimensions.get('window').width;
EStyleSheet.build({$rem: entireScreenWidth / 380});
const fontSizeValue = fontSize();
//common style for form elements

export default styles = EStyleSheet.create({
  //all screen background
  splashScreen: {
    width: '100%',
    height: '100%',
    alignContent: 'center',
    flex: 1,
    backgroundColor: '#0f1109',
  },
  screen: {
    width: '100%',
    height: '100%',
    alignContent: 'center',
    flex: 1,
    backgroundColor: 'white',
  },
  listingScreen: {
    width: '100%',
    height: '100%',
    alignContent: 'center',
    flex: 1,
    backgroundColor: '#d2d2d6',
  },
  toastStyle: {
    backgroundColor: 'rgba(0,0,0,0.7)',
    borderRadius: '30rem',
    paddingTop: '10rem',
    paddingBottom: '10rem',
    paddingLeft: '20rem',
    paddingRight: '20rem',
  },
  error: {
    color: '#ff6e6e',
    fontFamily:Platform.OS == 'android' ? 'Calisto-Regular' : 'Calisto MT',
    fontSize: fontSizeValue * 9,
    marginBottom: '5rem',
  },
  loading: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    backgroundColor: 'rgba(0,0,0,0.3)',
    justifyContent: 'center',
    alignItems: 'center',
    zIndex: 2000,
  },
  footerOverlay: {zIndex: 0},
});
