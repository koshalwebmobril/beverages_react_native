import React, {Component, Fragment} from 'react';
import {
  ImageBackground,
  Dimensions,
  SafeAreaView,
  StatusBar,
  View,
  Text,
  TextInput,
  Image,
  ScrollView,
  ActivityIndicator,
  Platform,
  BackHandler
} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import {connect} from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import STRINGS from '../utils/strings';
import Toast, {DURATION} from 'react-native-easy-toast';
import OTPInputView from '@twotalltotems/react-native-otp-input';
import {fontSize} from '../components/global/Fontsize';
import {TouchableOpacity} from 'react-native-gesture-handler';
import Styles from '../styles/styles';
import * as LOGINAPI from '../services/api-services/login-api';
import CONFIG from '../config/config';
import NavigationBar from 'react-native-navbar-color'

const screenWidth = Dimensions.get ('window').width;
EStyleSheet.build ({$rem: screenWidth / 380});
const fontSizeValue = fontSize ();

class otp extends Component {
  constructor (props) {
    super (props);
    this.state = {
      otpCode: '',
      loader: false,
      previousRoute: '',
      emailOrNumber: '',
      userId: '',
      isOTPVerified: false,
      count: 0
    };
  }

  componentDidMount = async () => {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
    NavigationBar.setStatusBarColor('white',false)
    NavigationBar.setColor('white');
    const {navigation} = this.props;
    var value = navigation.getParam('value', null);
    this.setState({emailOrNumber: value})
    setTimeout (() => {
      this.setState ({isOTPVerified: true});
    }, 2000);
  };

  componentWillUnmount () {
    // Remove the event listener
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
  }

  handleBackButton = () => {
    let count = this.state.count;
    if (count == 1) {
      BackHandler.exitApp ();
      return true;
    } else {
      this.showToast ('Press back again to leave');
      this.setState ({count: 1});
      return true;
    }
  };

  showToast = toastMsg => {
    this.toast.show (toastMsg);
  };

  verifyOTP = () => {
    if (this.state.otpCode !== '') {
      this.setState ({loader: true});
      let otpCode = this.state.otpCode;
      let payload = {
        otp: otpCode,
      };
      LOGINAPI.postSignupData (payload, CONFIG.BASE_URL + CONFIG.VERIFY_OTP_URL)
        .then (
          function (res) {
            this.setState ({loader: false});
            if (res != undefined) {
              let code = res.code;
              switch (code) {
                case CONFIG.statusOk:
                  AsyncStorage.setItem ('isOTPVerified', JSON.stringify (true));
                  let msg = res.message;
                  let userId = res.userid;
                  this.setState ({userId: userId});
                  this.showToast (msg);
                  setTimeout (() => {
                    this.props.navigation.navigate (
                      'ResetPassword',
                      {
                        userId: userId,
                      },
                      500
                    );
                  });
                  break;
                case CONFIG.statusError:
                  let error = res.message;
                  this.showToast(error);
              }
            } else {
              this.showToast ('Something went wrong. Please try again!');
            }
          }.bind (this)
        )
        .catch (
          function (err) {
            this.setState ({loader: false});
            this.showToast ('Something went wrong. Please try again!');
          }.bind (this)
        );
    } else {
      this.showToast ('OTP cannot be empty');
    }
  };

  sendOtp = () => {
    this.setState ({loader: true});
    let emailOrNumber = this.state.emailOrNumber;
    let payload = {
      mobile_number: emailOrNumber,
    };
    LOGINAPI.postSignupData (payload, CONFIG.BASE_URL + CONFIG.FORGOT_PASS_URL)
      .then (
        function (res) {
          this.setState ({loader: false});
          console.warn('res: ',res);
          if (res != undefined) {
            let code = res.code;
            switch (code) {
              case CONFIG.statusOk:
                this.setState({otpCode: ''});
                this.showToast ('OTP has been sent successfully');
                break;
              case CONFIG.statusUnauthorized:
                this.showToast ('User does not exists with these details');
                break;
              case CONFIG.statusError:
                this.showToast ('Something went wrong. Please try again!');
                break;
            }
          } else {
            this.showToast ('Something went wrong. Please try again!');
          }
        }.bind (this)
      )
      .catch (
        function (err) {
          console.warn('err : ',err);
          this.setState ({loader: false});
          this.showToast ('Something went wrong. Please try again!');
        }.bind (this)
      );
  };

  render () {
    return (
      <Fragment>
        {/* <SafeAreaView style={Styles.screen}> */}
        <View style={[Styles.screen]}>
         <SafeAreaView style={[{backgroundColor:'#47170d'}]} />
          <StatusBar backgroundColor="white" barStyle="dark-content" />
          {this.state.loader &&
            <View style={Styles.loading}>
              <ActivityIndicator size="large" color="#47170d" />
            </View>}
          <ScrollView>
            <View>
              <Image
                source={require ('../assets/drink_a_drink_logo.png')}
                style={styles.logoStyle}
              />
            </View>
            <ImageBackground
              source={require ('../assets/Mugs2.png')}
              style={styles.imgBackground}
            >
              <View style={styles.header}>
                <Text style={styles.headerTextStyle}>{STRINGS.otp.otp}</Text>
              </View>
              <View style={styles.otpTextStyle}>
                <Text style={styles.otpText}>{STRINGS.otp.otpText}</Text>
              </View>
              <View style={styles.enterOTPHereStyle}>
                <Text style={styles.enterOTPHere}>
                  {STRINGS.otp.enterOTPHere}
                </Text>
              </View>
              <View style={styles.otpView}>
                <View style={styles.otpViewWidth}>
                  <OTPInputView
                    style={styles.otpInput}
                    pinCount={6}
                    code={this.state.otpCode} 
                    onCodeChanged={otpCode => {
                      this.setState ({otpCode});
                    }}
                    codeInputFieldStyle={styles.underlineStyleBase}
                    codeInputHighlightStyle={styles.underlineStyleHighLighted}
                  />
                </View>
              </View>
              <View>
                {this.state.isOTPVerified &&
                  <TouchableOpacity
                    style={styles.resendSection}
                    onPress={this.sendOtp}
                  >
                    <Text style={styles.resendBtnTxt}>
                      {STRINGS.forgotPassword.resendOtp}
                    </Text>
                  </TouchableOpacity>}
              </View>
              <View style={styles.submitSection}>
                <TouchableOpacity
                  style={styles.submitBtn}
                  onPress={this.verifyOTP}
                >
                  <Text style={styles.submitText}>
                    {STRINGS.forgotPassword.submit + ' '} 
                  </Text>
                  <Image
                      source={require ('../assets/arrow.png')}
                      style={styles.arrowIcon}
                    />
                </TouchableOpacity>
              </View>
            </ImageBackground>
          </ScrollView>
          <Toast
            position="bottom"
            style={Styles.toastStyle}
            textStyle={{color: 'white'}}
            ref={ref => {
              this.toast = ref;
            }}
          />
          </View>
        {/* </SafeAreaView> */}
      </Fragment>
    );
  }
}

const mapStateToProps = state => {
  const {userReducer} = state;
  return {userReducer};
};

const mapDispatchToProps = dispatch => {
  return {
    updateUserDetails: profileDetails => {
      dispatch (updateUserDetails (profileDetails));
    },
  };
};
export default connect (mapStateToProps, mapDispatchToProps) (otp);

const styles = EStyleSheet.create ({
  imgBackground: {
    flex: 1,
    width: null,
    height: null,
  },
  logoStyle: {
    height: '125rem',
    width: '327rem',
    margin: '30rem',
  },
  header: {
    position: 'relative',
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop: '30rem',
  },
  headerTextStyle: {
    fontSize: fontSizeValue * 23,
    fontFamily: Platform.OS == 'android' ? 'Calisto-Regular' : 'Calisto MT',
    color: '#47170d',
  },
  otpTextStyle: {
    width: '75%',
    alignItems: 'center',
    marginTop: '40rem',
    marginLeft: '45rem',
    flexDirection: 'row',
    justifyContent: 'center',
  },
  otpText: {
    fontSize: fontSizeValue * 10,
    fontFamily: Platform.OS == 'android' ? 'Calisto-Regular' : 'Calisto MT',
    color: '#000000',
    textAlign: 'center',
  },
  enterOTPHereStyle: {
    flexDirection: 'row',
    width: '100%',
    justifyContent: 'center',
    marginTop: '30rem',
  },
  enterOTPHere: {
    fontSize: fontSizeValue * 10,
    fontFamily: 'Calisto-Bold',
    fontWeight:'bold',
    color: '#000000',
  },
  otpView: {
    flexDirection: 'row',
    marginTop: '25rem',
    justifyContent: 'center',
  },
  otpViewWidth: {
    width: '85%',
  },
  underlineStyleBase: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    width: '42rem',
    height: '42rem',
    borderWidth: 2,
    borderColor: 'black',
    borderRadius: '6rem',
    color: 'black',
    fontSize: fontSizeValue * 12,
    fontFamily: Platform.OS == 'android' ? 'Calisto-Regular' : 'Calisto MT',
  },
  otpInput: {
    width: '100%',
    height: '100rem',
  },
  submitSection: {
    height: '46rem',
    marginTop: '30rem',
    justifyContent: 'center',
    alignItems: 'center',
  },
  resendSection: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  submitBtn: {
    width: '142rem',
    height: '46rem',
    backgroundColor: '#47170d',
    borderRadius: '6rem',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    padding:'12rem'

  },
  resendBtnTxt: {
    fontSize: fontSizeValue * 10,
    fontFamily: Platform.OS == 'android' ? 'Calisto-Regular' : 'Calisto MT',
    color: '#47170d',
    textDecorationLine: 'underline',
  },
  submitText: {
    fontSize: fontSizeValue * 14,
    fontFamily: Platform.OS == 'android' ? 'Calisto-Regular' : 'Calisto MT',
    color: 'white',
  },
  arrowIcon: {
    width: '25rem',
    height: '25rem',
  },
});
