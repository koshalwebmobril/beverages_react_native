// import React, { Component } from 'react';
// import { View, Alert, KeyboardAvoidingView, PermissionsAndroid,Dimensions, Platform, StyleSheet, Modal, ImageBackground, ScrollView, Image, Text, TextInput,TouchableWithoutFeedback ,TouchableOpacity,Keyboard, ActivityIndicator} from 'react-native';
// import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
// import RNHTMLtoPDF from 'react-native-html-to-pdf';
// import { background, logo } from '../utility/Icons';
// import { Container, Header, Content, Picker, Form, Left ,Input} from "native-base";
// import FontAwesome from "react-native-vector-icons/dist/FontAwesome";
// import { validateFields, validateDate } from '../utility/Validation';
// import CustomTextInput from '../components/CustomTextInput';
// var widthhh = Dimensions.get('window').width
// var heighttt = Dimensions.get('window').height
// var RNFS = require('react-native-fs');
// import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview'

// export default class BuildingScreen1 extends Component {
//   constructor(props) {
//     super(props);
//     this.state = {
//       loading: false,
//       items: [],
//       pdf_data: [],
//       selected: '',
//       value: '',

    

//       /*------------------------ Supt -------------------------------------------------- */
     
//       supt_rating: '',

//       /* ------------------------- Exterior ------------------------------------------ */
//       exterior_cond_sidewalk_steps_rating: '',
//       exterior_cond_cans_cover_rating: '',
//       exterior_cond_doors_rating: '',
//       exterior_cond_courtyard_rating: '',
//       exterior_cond_yard_drain_rating: '',
//       exterior_cond_fountain_walls_rating: '',
//       exterior_cond_lights_at_entry_rating: '',
//       exterior_cond_lights_at_yard_rating: '',
//       /*---------------------Entrance --------------------------------------------- */
//       entrance_entrance_door_rating: '',
//        entrance_sidelight_rating: '',
//       entrance_intercom_rating: '',
//       entrance_mailboxes_rating: '',
//       entrance_floor_rating: '',
//      entrance_walls_rating: '',
//        entrance_lightning_rating: '',

//       /*-----------------Stairwells------------------------ */
//       stairwells_steps_treads_rating: '',
//       stairwells_walls_rating: '',
//        stairwells_lightning_rating: '',
//       stairwells_emergency_lightning_rating: '',
//        stairwells_doors_rating: '',
//        stairwells_windows_rating: '',
//       stairwells_signs_rating: '',

//       /*----------------------Emergency------------------- */
    
//       emergency_exit_lightning_rating: '',
//       emergency_exit_floor_rating: '',
//       emergency_exit_doors_rating: '',
//       emergency_exit_signs_rating: '',

//       /*--------------------Roof ---------------------------- */
    
//       roof_bulkhead_rating: '',
//       roof_roof_doors_rating: '',
//       roof_roof_surface_rating: '',
//       roof_gutter_rating: '',
//       roof__leaders_rating: '',
//       roof_skylight_rating: '',
//       roof_parapet_rating: '',

//       /*------------------------------ fire escapes ----------------------------------- */

//       fire_escapes_ladders_rating: '',
//       fire_escapes_basket_rating: '',
//       fire_escapes_railling_rating: '',
//       fire_escapes_ladder_shoes_rating: '',

//       /*----------------------------- Lobbies -------------------------------------------- */

//       lobbies_floor_rating: '',
//       lobbies_walls_rating: '',
//       lobbies_lightning_emergency_rating: '',
//       lobbies_windows_sills_rating: '',

//       /* --------------------------------- basement ----------------------------------------------- */
     
//       basement_doors_rating: '',
//       basement_stairs_rating: '',
//       basement_floor_rating: '',
//       basement_walls_rating: '',
//       basement_lightning_rating: '',
//       basement_windows_rating: '',
//       basement_garbage_room_rating: '',

//       /*--------------------------------------- Laundry Room --------------------------------------- */

//       laundry_room_equipment_rating: '',
//       laundry_room_floor_rating: '',
//       laundry_room_walls_rating: '',
//       laundry_room_lightning_rating: '',

//       /*------------------------------------ Boiler------------------------------------------------- */

//       boiler_fuel_supply_rating: '',
//       boiler_gauge_color_coding_markings_rating: '',
//       boiler_nurners_rating: '',
//       boiler_coils_rating: '',
//       boiler_oil_tank_rating: '',
//       boiler_plumbing_rating: '',
//       boiler_leaks_rating: '',
//       boiler_overheads_returns_rating: '',

//       /*------------------------------------ Meters ------------------------------------------------------- */

//       meters_lightning_rating: '',
//       meters_accessibility_rating: '',
//       meters_up_keeping_rating: '',

//       /*--------------------------- Elevator ------------------------------------------------------ */

//       elevator_lightning_rating: '',
//       elevator_signals_rating: '',
//       elevator_doors_rating: '',
//       elevator_cab_floor_rating: '',
//       elevator_cab_walls_rating: '',
//       elevator_cab_ceilling_rating: '',
//       elevator_floor_nmber_on_doors_rating: '',

//       /* -------------------------------------------------Fire Equipments --------------------------------------- */
     
//       fire_equipment_sprinklers_rating: '',
//       fire_equipment_extinguishers_rating: '',
//       fire_equipment_alarm_system_rating: '',

//       /*-------------------------------------- Vacant---------------------------------------- */
      
//       vacant_units_apt1_rating: '',
//       vacant_units_apt2_rating: '',
//       inventory_rating: '',
//       selectedChooseRating: '',

//       bottomHeight:0


//     }
//   }

//   testIOS =()=>{
//       RNFS.readDir(RNFS.DocumentDirectoryPath).then(files => {
//       //console.log('--->>>>>>',files)
//       Alert.alert(JSON.stringify(files))
//   })
//   .catch(err => {
    
//      // console.log(err.message, err.code);
     
//   });
//   }

//   componentDidMount(){ 

//     this.DropDownData_Api();

//     this.keyboardDidShowListener = Keyboard.addListener(
//       'keyboardDidShow',
//       this._keyboardDidShow,
//     );
//     this.keyboardDidHideListener = Keyboard.addListener(
//       'keyboardDidHide',
//       this._keyboardDidHide,
//     );
//   }

//   componentWillUnmount() {
//     this.keyboardDidShowListener.remove();
//     this.keyboardDidHideListener.remove();
//   }

//   _keyboardDidShow =() =>{
//       this.setState({bottomHeight:310})
//   }

//   _keyboardDidHide =() => {
//     this.setState({bottomHeight:0})
//   }
 
//   DropDownData_Api = () => {
//     this.setState({ loading: true })
//     try {
//       const siteUrl = "http://webmobril.org/dev/inspectionapp/api/ratings";
//       fetch(siteUrl)
//         .then(response => response.json())
//         .then((resp) => {
//           if (resp.code == 200) {
//             //  this.setState({loading:false})   
//             var temp = [] 
//             if(Platform.OS == 'andorid')  resp.data // not remove becayse it act as placeholder in android
//             else if  (Platform.OS == 'ios')  resp.data.shift() // will remove first element 
//             console.log(resp.data)
//             this.setState({ items:resp.data }, () => this.setState({ loading: false }))
//        //, () => this.setState({ loading: false })
//           }
//           else {
//             this.setState({ loading: false })
//             alert("Oops! Please check network connection.")
//           }
//         }).catch((e) => {
//           this.setState({ loading: false })
//          // console.log("error=->", e)
//         })
//     }
//     catch (err) {
//       this.setState({ loading: false })
//      // console.log("Exeption error=-=>>", err)
//     }
//   }
//   onValueChange = (value, name) => {
//     /* --------------------- Exterior Rating ----------------------- */

//     if (name == "Rating0") {
//       this.setState({ supt_rating: value })
//      // console.log('----------', this.state.supt_rating)
//     }
//     else if (name == "Rating1") {
//       this.setState({ exterior_cond_sidewalk_steps_rating: value })
//       console.log('----------', this.state.exterior_cond_sidewalk_steps_rating)
//     }
//     else if (name == "Rating2") {
//       this.setState({ exterior_cond_cans_cover_rating: value })
//       console.log('----------', this.state.exterior_cond_cans_cover_rating)
//     }
//     else if (name == "Rating3") {
//       this.setState({ exterior_cond_doors_rating: value })
//       console.log('----------', this.state.exterior_cond_doors_rating)
//     }
//     else if (name == "Rating4") {
//       this.setState({ exterior_cond_courtyard_rating: value })
//       console.log('----------', this.state.exterior_cond_courtyard_rating)
//     }
//     else if (name == "Rating5") {
     
//       this.setState({ exterior_cond_yard_drain_rating: value })
//       console.log('----------', this.state.exterior_cond_yard_drain_rating)
//     }
//     else if (name == "Rating6") {
     
//       this.setState({ exterior_cond_fountain_walls_rating: value })
//       console.log(this.state.exterior_cond_fountain_walls_rating)
//     }
//     else if (name == "Rating7") {
     
//       this.setState({ exterior_cond_lights_at_entry_rating: value })
//       console.log('----------', this.state.exterior_cond_lights_at_entry_rating)
//     }
//     else if (name == "Rating8") {
     
//       this.setState({ exterior_cond_lights_at_yard_rating: value })
//       console.log('----------', this.state.exterior_cond_lights_at_yard_rating)
//     }
//     /*-------------------------- Entrance Condition----------------------- */

//     else if (name == "Rating9") {
     
//       this.setState({ entrance_entrance_door_rating: value })
//     }

//     else if (name == "Rating10") {
     
//       this.setState({ entrance_sidelight_rating: value })
//     }
//     else if (name == "Rating11") {
     
//       this.setState({ entrance_intercom_rating: value })
//     }
//     else if (name == "Rating12") {
     
//       this.setState({ entrance_mailboxes_rating: value })
//     }
//     else if (name == "Rating13") {
     
//       this.setState({ entrance_floor_rating: value })
//     }
//     else if (name == "Rating14") {
     
//       this.setState({ entrance_walls_rating: value })
//     }

//     /*------------------------------- stairwells_Rating -------------------------- */
//     else if (name == "Rating15") {
//       this.setState({ stairwells_steps_treads_rating: value })

//     }
//     else if (name == "Rating16") {
//       this.setState({ stairwells_walls_rating: value })
//     }
//     else if (name == "Rating17") {
//       this.setState({ stairwells_lightning_rating: value })
//     }
//     else if (name == "Rating18") {
//       this.setState({ stairwells_emergency_lightning_rating: value })
//     }
//     else if (name == "Rating19") {
//       this.setState({ stairwells_doors_rating: value })
//     }
//     else if (name == "Rating20") {
//       this.setState({ stairwells_windows_rating: value })
//     }
//     else if (name == "Rating21") {
//       this.setState({ stairwells_signs_rating: value })
//     }
//     /*------------------------------- Emergency Rating------------------------ */

//     else if (name == "Rating22") {
//       this.setState({ emergency_exit_lightning_rating: value })
//     }
//     else if (name == "Rating23") {
//       this.setState({ emergency_exit_floor_rating: value })
//     }
//     else if (name == "Rating24") {
//       this.setState({ emergency_exit_doors_rating: value })
//     }
//     else if (name == "Rating25") {
//       this.setState({ emergency_exit_signs_rating: value })
//     }

//     /*------------------------------- Roof Rating------------------------ */
//     else if (name == "Rating26") {
//       this.setState({ roof_bulkhead_rating: value })

//     } else if (name == "Rating27") {
//       this.setState({ roof_roof_doors_rating: value })
//     }
//     else if (name == "Rating28") {
//       this.setState({ roof_roof_surface_rating: value })
//     }
//     else if (name == "Rating29") {
//       this.setState({ roof_gutter_rating: value })
//     }
//     else if (name == "Rating30") {
//       this.setState({ roof__leaders_rating: value })
//     } else if (name == "Rating31") {
//       this.setState({ roof_skylight_rating: value })
//     }
//     else if (name == "Rating32") {
//       this.setState({ roof_parapet_rating: value })
//     }
//     else if (name == "Rating33") {
//       this.setState({ fire_escapes_ladders_rating: value })
//     }
//     else if (name == "Rating34") {
//       this.setState({ fire_escapes_basket_rating: value })
//     }
//     else if (name == "Rating35") {
//       this.setState({ fire_escapes_railling_rating: value })
//     }
//     else if (name == "Rating36") {
//       this.setState({ fire_escapes_ladder_shoes_rating: value })
//     }
//     else if (name == "Rating37") {
//       this.setState({ lobbies_floor_rating: value })
//     }
//     else if (name == "Rating38") {
//       this.setState({ lobbies_walls_rating: value })
//     }
//     else if (name == "Rating39") {
//       this.setState({ lobbies_lightning_emergency_rating: value })
//     }
//     else if (name == "Rating40") {
//       this.setState({ lobbies_windows_sills_rating: value })
//     }
//     else if (name == "Rating40") {
//       this.setState({ basement_doors_rating: value })
//     }
//     else if (name == "Rating41") {
//       this.setState({ basement_stairs_rating: value })
//     }
//     else if (name == "Rating42") {
//       this.setState({ basement_floor_rating: value })
//     }
//     else if (name == "Rating43") {
//       this.setState({ basement_walls_rating: value })
//     }
//     else if (name == "Rating44") {
//       this.setState({ basement_lightning_rating: value })
//     }
//     else if (name == "Rating45") {
//       this.setState({ basement_windows_rating: value })
//     }
//     else if (name == "Rating46") {
//       this.setState({ basement_garbage_room_rating: value })
//     }
//     else if (name == "Rating47") {
//       this.setState({ laundry_room_equipment_rating: value })
//     }
//     else if (name == "Rating48") {
//       this.setState({ laundry_room_floor_rating: value })
//     }
//     else if (name == "Rating49") {
//       this.setState({ laundry_room_walls_rating: value })
//     }
//     else if (name == "Rating50") {
//       this.setState({ laundry_room_lightning_rating: value })
//     }
//     else if (name == "Rating51") {
//       this.setState({ boiler_fuel_supply_rating: value })
//     }
//     else if (name == "Rating52") {
//       this.setState({ boiler_gauge_color_coding_markings_rating: value })
//     }
//     else if (name == "Rating53") {
//       this.setState({ boiler_nurners_rating: value })
//     }
//     else if (name == "Rating54") {
//       this.setState({ boiler_oil_tank_rating: value })
//     }
//     else if (name == "Rating55") {
//       this.setState({ boiler_plumbing_rating: value })
//     }
//     else if (name == "Rating56") {
//       this.setState({ boiler_leaks_rating: value })
//     }
//     else if (name == "Rating57") {
//       this.setState({ boiler_overheads_returns_rating: value })
//     }

//     else if (name == "Rating58") {
//       this.setState({ meters_lightning_rating: value })
//     }
//     else if (name == "Rating59") {
//       this.setState({ meters_accessibility_rating: value })
//     }
//     else if (name == "Rating60") {
//       this.setState({ meters_up_keeping_rating: value })
//     }

//     else if (name == "Rating61") {
//       this.setState({ elevator_lightning_rating: value })
//     }
//     else if (name == "Rating62") {
//       this.setState({ elevator_signals_rating: value })
//     }
//     else if (name == "Rating63") {
//       this.setState({ elevator_doors_rating: value })
//     }
//     else if (name == "Rating64") {
//       this.setState({ elevator_cab_floor_rating: value })
//     }
//     else if (name == "Rating65") {
//       this.setState({ elevator_cab_walls_rating: value })
//     }
//     else if (name == "Rating66") {
//       this.setState({ elevator_cab_ceilling_rating: value })
//     }
//     else if (name == "Rating67") {
//       this.setState({ elevator_floor_nmber_on_doors_rating: value })
//     }

//     else if (name == "Rating68") {
//       this.setState({ fire_equipment_sprinklers_rating: value })
//     }
//     else if (name == "Rating69") {
//       this.setState({ fire_equipment_extinguishers_rating: value })
//     }
//     else if (name == "Rating70") {
//       this.setState({ fire_equipment_alarm_system_rating: value })
//     }

//     /*-----------------------------Vacant---------------------------------------- */

//     else if (name == "Rating71") {

//       this.setState({ vacant_units_apt1_rating: value })
//     }
//     else if (name == "Rating72") {
//       this.setState({ vacant_units_apt2_rating: value })
//     }

//     /*--------------------- Inventory -------------------------------------------- */
//     else if (name == "Rating73") {
//       this.setState({ inventory_rating: value })
//     }

//     else {
//       alert("Please choose rating.")
//     }
//   }

//   AskPermissionGalary = () => {
//     console.log("AskPermissionGalary fn called")
//     var that = this;
//     //Checking for the permission just after component loaded
//     async function requestGalaryPermission() {
//       //Calling the permission function
//       //   android.permission.WRITE_EXTERNAL_STORAGE
//       const granted = await PermissionsAndroid.request(
//         PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
//         {
//           title: 'App Galary Permission.',
//           message: 'App needs access to your Galary.',
//         }
//       );
//       if (granted === PermissionsAndroid.RESULTS.GRANTED) {
//         that.createPDF();
//       } else {
//         alert('Galary Permission Denied.');
//       }
//     }
//     if (Platform.OS === 'android') {
//       requestGalaryPermission();
//     } else {
//       this.createPDF();
//     }
//   }
//   saveDataIOS = async (from,name) => {
//     console.log('yess IOSSSS----------')
//     Alert.alert("PDF file saved succcessfully " + name );
  
//   }
//    saveData = async (from) => {
//     try {
//       const granted = await PermissionsAndroid.requestMultiple([
//         PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
//         PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
//       ]);
//     } catch (err) {
//       Alert.alert("Write permissions have not been granted,go to settings to enable it." );
//     }
//     const readGranted = await PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE); 
//     const writeGranted = await PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE);
//     if(!readGranted || !writeGranted) {
//       console.log('Read and write permissions have not been granted');
//       Alert.alert("Read and write permissions have not been granted" );
//       return;
//     }
//     let r = Math.random().toString(36).substring(7);
//     let name = 'building_' + r.toString()
   
//     var path = `${RNFS.ExternalStorageDirectoryPath}/BuildingIspection`;
//     RNFS.mkdir(path);
//     path += '/'+ name+ '.pdf';
//     RNFS.moveFile(from, path)
//       .then((success) => {
//         console.log('Success',path);
//         Alert.alert("PDF file saved succcessfully " + name );
//       })
//       .catch((err) => {
//         console.log(err.message);
//         Alert.alert(err.message)
//       });
//   }
//   createPDF = async () => {
//     let body = {
//       'building': this.buildingInput.getInputValue(),
//       "week_of": this.week_ofInput.getInputValue(),
//       "manager": this.managerInput.getInputValue(),
//       "supt": this.suptInput.getInputValue(),
//       "rating_choose_from": this.state.supt_rating,

//       "exterior_cond_sidewalk_steps": this.exterior_cond_sidewalk_stepsInput.getInputValue(),
//       "exterior_cond_sidewalk_steps_rating": this.state.exterior_cond_sidewalk_steps_rating,

//       "exterior_cond_cans_cover": this.exterior_cond_cans_coverInput.getInputValue(),
//       "exterior_cond_cans_cover_rating": this.state.exterior_cond_cans_cover_rating,

//       "exterior_cond_doors": this.exterior_cond_doorsInput.getInputValue(),
//       "exterior_cond_doors_rating": this.state.exterior_cond_doors_rating,

//       "exterior_cond_courtyard": this.exterior_cond_courtyardInput.getInputValue(),
//       "exterior_cond_courtyard_rating": this.state.exterior_cond_courtyard_rating,

//       "exterior_cond_yard_drain": this.exterior_cond_yard_drainInput.getInputValue(),
//       "exterior_cond_yard_drain_rating": this.state.exterior_cond_yard_drain_rating,

//       "exterior_cond_fountain_walls": this.exterior_cond_fountain_wallsInput.getInputValue(),
//       "exterior_cond_fountain_walls_rating": this.state.exterior_cond_fountain_walls_rating,

//       "exterior_cond_lights_at_entry": this.exterior_cond_lights_at_entryInput.getInputValue(),
//       "exterior_cond_lights_at_entry_rating": this.state.exterior_cond_lights_at_entry_rating,

//       "exterior_cond_lights_at_yard": this.exterior_cond_lights_at_yardInput.getInputValue(),
//       "exterior_cond_lights_at_yard_rating": this.state.exterior_cond_lights_at_yard_rating,

//       "entrance_entrance_door": this.entrance_entrance_doorInput.getInputValue(),
//       "entrance_entrance_door_rating": this.state.entrance_entrance_door_rating,

//       "entrance_sidelight": this.entrance_sidelightInput.getInputValue(),
//       "entrance_sidelight_rating": this.state.entrance_sidelight_rating,

//       "entrance_intercom": this.entrance_intercomInput.getInputValue(),
//       "entrance_intercom_rating": this.state.entrance_intercom_rating,

//       "entrance_mailboxes": this.entrance_mailboxesInput.getInputValue(),
//       "entrance_mailboxes_rating": this.state.entrance_mailboxes_rating,

//       "entrance_floor": this.entrance_floorInput.getInputValue(),
//       "entrance_floor_rating": this.state.entrance_floor_rating,

//       "entrance_walls": this.entrance_wallsInput.getInputValue(),
//       "entrance_walls_rating": this.state.entrance_walls_rating,

//       "entrance_lightning": this.entrance_lightningInput.getInputValue(),
//       "entrance_lightning_rating": this.state.entrance_lightning_rating,

//       "stairwells_steps_treads": this.stairwells_steps_treadsInput.getInputValue(),
//       "stairwells_steps_treads_rating": this.state.stairwells_steps_treads_rating,

//       "stairwells_walls": this.stairwells_wallsInput.getInputValue(),
//       "stairwells_walls_rating": this.state.stairwells_walls_rating,

//       "stairwells_lightning": this.stairwells_lightningInput.getInputValue(),
//       "stairwells_lightning_rating": this.state.stairwells_lightning_rating,

//       "stairwells_emergency_lightning": this.stairwells_emergency_lightningInput.getInputValue(),
//       "stairwells_emergency_lightning_rating": this.state.stairwells_emergency_lightning_rating,

//       "stairwells_doors": this.stairwells_doorsInput.getInputValue(),
//       "stairwells_doors_rating": this.state.stairwells_doors_rating,

//       "stairwells_windows": this.stairwells_windowsInput.getInputValue(),
//       "stairwells_windows_rating": this.state.stairwells_windows_rating,

//       "stairwells_signs": this.stairwells_signsInput.getInputValue(),
//       "stairwells_signs_rating": this.state.stairwells_signs_rating,

//       "emergency_exit_lightning": this.emergency_exit_lightningInput.getInputValue(),
//       "emergency_exit_lightning_rating": this.state.emergency_exit_lightning_rating,

//       "emergency_exit_floor": this.emergency_exit_floorInput.getInputValue(),
//       "emergency_exit_floor_rating": this.state.emergency_exit_floor_rating,

//       "emergency_exit_doors": this.emergency_exit_doorsInput.getInputValue(),
//       "emergency_exit_doors_rating": this.state.emergency_exit_doors_rating,

//       "emergency_exit_signs": this.emergency_exit_signsInput.getInputValue(),
//       "emergency_exit_signs_rating": this.state.emergency_exit_signs_rating,

//       "roof_bulkhead": this.roof_bulkheadInput.getInputValue(),
//       "roof_bulkhead_rating": this.state.roof_bulkhead_rating,

//       "roof_roof_doors": this.roof_roof_doorsInput.getInputValue(),
//       "roof_roof_doors_rating": this.state.roof_roof_doors_rating,

//       "roof_roof_surface": this.roof_roof_surfaceInput.getInputValue(),
//       "roof_roof_surface_rating": this.state.roof_roof_surface_rating,

//       "roof_gutter": this.roof_gutterInput.getInputValue(),
//       "roof_gutter_rating": this.state.roof_gutter_rating,

//       "roof__leaders": this.roof__leadersInput.getInputValue(),
//       "roof__leaders_rating": this.state.roof__leaders_rating,

//       "roof_skylight": this.roof_skylightInput.getInputValue(),
//       "roof_skylight_rating": this.state.roof_skylight_rating,

//       "roof_parapet": this.roof_parapetInput.getInputValue(),
//       "roof_parapet_rating": this.state.roof_parapet_rating,
//       "fire_escapes_ladders": this.fire_escapes_laddersInput.getInputValue(),
//       "fire_escapes_ladders_rating": this.state.fire_escapes_ladders_rating,

//       "fire_escapes_basket": this.fire_escapes_basketInput.getInputValue(),
//       "fire_escapes_basket_rating": this.state.fire_escapes_basket_rating,

//       "fire_escapes_railling": this.fire_escapes_raillingInput.getInputValue(),
//       "fire_escapes_railling_rating": this.state.fire_escapes_railling_rating,

//       "fire_escapes_ladder_shoes": this.fire_escapes_ladder_shoesInput.getInputValue(),
//       "fire_escapes_ladder_shoes_rating": this.state.fire_escapes_ladder_shoes_rating,

//       "lobbies_floor": this.lobbies_floorInput.getInputValue(),
//       "lobbies_floor_rating": this.state.lobbies_floor_rating,

//       "lobbies_walls": this.lobbies_wallsInput.getInputValue(),
//       "lobbies_walls_rating": this.state.lobbies_walls_rating,

//       "lobbies_lightning_emergency": this.lobbies_lightning_emergencyInput.getInputValue(),
//       "lobbies_lightning_emergency_rating": this.state.lobbies_lightning_emergency_rating,

//       "lobbies_windows_sills": this.lobbies_windows_sillsInput.getInputValue(),
//       "lobbies_windows_sills_rating": this.state.lobbies_windows_sills_rating,

//       "basement_doors": this.basement_doorsInput.getInputValue(),
//       "basement_doors_rating": this.state.basement_doors_rating,

//       "basement_stairs": this.basement_stairsInput.getInputValue(),
//       "basement_stairs_rating": this.state.basement_stairs_rating,

//       "basement_floor": this.basement_floorInput.getInputValue(),
//       "basement_floor_rating": this.state.basement_floor_rating,

//       "basement_walls": this.basement_wallsInput.getInputValue(),
//       "basement_walls_rating": this.state.basement_walls_rating,

//       "basement_lightning": this.basement_lightningInput.getInputValue(),
//       "basement_lightning_rating": this.state.basement_lightning_rating,

//       "basement_windows": this.basement_windowsInput.getInputValue(),
//       "basement_windows_rating": this.state.basement_windows_rating,

//       "basement_garbage_room": this.basement_garbage_roomInput.getInputValue(),
//       "basement_garbage_room_rating": this.state.basement_garbage_room_rating,

//       "laundry_room_equipment": this.laundry_room_equipmentInput.getInputValue(),
//       "laundry_room_equipment_rating": this.state.laundry_room_equipment_rating,

//       "laundry_room_floor": this.laundry_room_floorInput.getInputValue(),
//       "laundry_room_floor_rating": this.state.laundry_room_floor_rating,

//       "laundry_room_walls": this.laundry_room_wallsInput.getInputValue(),
//       "laundry_room_walls_rating": this.state.laundry_room_walls_rating,

//       "laundry_room_lightning": this.laundry_room_lightningInput.getInputValue(),
//       "laundry_room_lightning_rating": this.state.laundry_room_lightning_rating,

//       "boiler_fuel_supply": this.boiler_fuel_supplyInput.getInputValue(),
//       "boiler_fuel_supply_rating": this.state.boiler_fuel_supply_rating,

//       "boiler_gauge_color_coding_markings": this.boiler_gauge_color_coding_markingsInput.getInputValue(),
//       "boiler_gauge_color_coding_markings_rating": this.state.boiler_gauge_color_coding_markings_rating,


//       "boiler_nurners": this.boiler_nurnersInput.getInputValue(),
//       "boiler_nurners_rating": this.state.boiler_nurners_rating,

//       "boiler_coils": this.boiler_coilsInput.getInputValue(),
//       "boiler_coils_rating": this.state.boiler_coils_rating,

//       "boiler_oil_tank": this.boiler_oil_tankInput.getInputValue(),
//       "boiler_oil_tank_rating": this.state.boiler_oil_tank_rating,

//       "boiler_plumbing": this.boiler_plumbingInput.getInputValue(),
//       "boiler_plumbing_rating": this.state.boiler_plumbing_rating,

//       "boiler_leaks": this.boiler_leaksInput.getInputValue(),
//       "boiler_leaks_rating": this.state.boiler_leaks_rating,

//       "boiler_overheads_returns": this.boiler_overheads_returnsInput.getInputValue(),
//       "boiler_overheads_returns_rating": this.state.boiler_overheads_returns_rating,

//       "meters_lightning": this.meters_lightningInput.getInputValue(),
//       "meters_lightning_rating": this.state.meters_lightning_rating,

//       "meters_accessibility": this.meters_accessibilityInput.getInputValue(),
//       "meters_accessibility_rating": this.state.meters_accessibility_rating,

//       "meters_up_keeping": this.meters_up_keepingInput.getInputValue(),
//       "meters_up_keeping_rating": this.state.meters_up_keeping_rating,

//       "elevator_lightning": this.elevator_lightningInput.getInputValue(),
//       "elevator_lightning_rating": this.state.elevator_lightning_rating,

//       "elevator_signals": this.elevator_signalsInput.getInputValue(),
//       "elevator_signals_rating": this.state.elevator_signals_rating,

//       "elevator_doors": this.elevator_doorsInput.getInputValue(),
//       "elevator_doors_rating": this.state.elevator_doors_rating,

//       "elevator_cab_floor": this.elevator_cab_floorInput.getInputValue(),
//       "elevator_cab_floor_rating": this.state.elevator_cab_floor_rating,

//       "elevator_cab_walls": this.elevator_cab_wallsInput.getInputValue(),
//       "elevator_cab_walls_rating": this.state.elevator_cab_walls_rating,

//       "elevator_cab_ceilling": this.elevator_cab_ceillingInput.getInputValue(),
//       "elevator_cab_ceilling_rating": this.state.elevator_cab_ceilling_rating,

//       "elevator_floor_nmber_on_doors": this.elevator_floor_nmber_on_doorsInput.getInputValue(),
//       "elevator_floor_nmber_on_doors_rating": this.state.elevator_floor_nmber_on_doors_rating,

//       "fire_equipment_sprinklers": this.fire_equipment_sprinklersInput.getInputValue(),
//       "fire_equipment_sprinklers_rating": this.state.fire_equipment_sprinklers_rating,

//       "fire_equipment_extinguishers": this.fire_equipment_extinguishersInput.getInputValue(),
//       "fire_equipment_extinguishers_rating": this.state.fire_equipment_extinguishers_rating,

//       "fire_equipment_alarm_system": this.fire_equipment_alarm_systemInput.getInputValue(),
//       "fire_equipment_alarm_system_rating": this.state.fire_equipment_alarm_system_rating,


//       "vacant_units_apt1": this.vacant_units_apt1Input.getInputValue(),
//       "vacant_units_apt1_rating": this.state.vacant_units_apt1_rating,

//       "vacant_units_apt2": this.vacant_units_apt2Input.getInputValue(),
//       "vacant_units_apt2_rating": this.state.vacant_units_apt2_rating,

//       "inventory": this.inventoryInput.getInputValue(),
//       "inventory_rating": this.state.inventory_rating,

//       "problem": this.problemInput.getInputValue(),
//       "notes": this.notesInput.getInputValue(),

//     }


//     //this.Call();
//     console.log("createPDF fn called")
//     var dta = this.state.pdf_data
//     // var String_data = JSON.stringify(dta)
//     var String_data = JSON.stringify(body)


//     console.log("PDF DATA_+=->>", String_data)

//     let a = []
//     Object.entries(body).forEach(function ([key, value]) {
//       a.push('<tr><td>' + `${key}` + '</td><td>' + `${value}` + '</td></tr>')
//     })

//     let r = Math.random().toString(36).substring(7);
//     let name = 'building_' + r.toString()
   
//     let options = {
//       html: `<p>
//           <h1 style="color:red;"><u>Building Inspection Details</u></h1></p>
//           <table border=1> ${a} </table>`,
//       fileName: name,
//       directory: 'Documents',
//     };

    

//    if(Platform.OS == 'android'){
//     try {
//       const granted = await PermissionsAndroid.requestMultiple([
//         PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
//         PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
//       ]);
//     } catch (err) {
//       Alert.alert("Write permissions have not been granted,go to settings to enable it." );
//     }
//     const readGranted = await PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE); 
//     const writeGranted = await PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE);
//     if(!readGranted || !writeGranted) {
//       console.log('Read and write permissions have not been granted');
//       Alert.alert("Read and write permissions have not been granted" );
//       return;
//     }
//    }

//     this.Call()
//     let file = await RNHTMLtoPDF.convert(options)
//     console.log("Pdf stored location=-=>>", file.filePath);
//     Platform.OS == 'android' ? this.saveData(file.filePath)
//     : this.saveDataIOS(file.filePath,name)

    
//   }

//   requestRunTimePermission = () => {
//     var that = this;
//     async function externalStoragePermission() {
//       try {
//         const granted = await PermissionsAndroid.request(
//           PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
//           {
//             title: 'External Storage Write Permission',
//             message: 'App needs access to Storage data.',
//           }
//         );
//         if (granted === PermissionsAndroid.RESULTS.GRANTED) {
//           that.createPDF_File();
//         } else {
//           alert('WRITE_EXTERNAL_STORAGE permission denied');
//         }
//       } catch (err) {
//         Alert.alert('Write permission err', err);
//         console.warn(err);
//       }
//     }

//     if (Platform.OS === 'android') {
//         externalStoragePermission();
//     } else {
//         this.createPDF_File();
//     }
//   }

//   async createPDF_File() {

//     let body = {
//       'building': this.buildingInput.getInputValue(),
//       "week_of": this.week_ofInput.getInputValue(),
//       "manager": this.managerInput.getInputValue(),
//       "supt": this.suptInput.getInputValue(),
//       "rating_choose_from": this.state.supt_rating,

//       "exterior_cond_sidewalk_steps": this.exterior_cond_sidewalk_stepsInput.getInputValue(),
//       "exterior_cond_sidewalk_steps_rating": this.state.exterior_cond_sidewalk_steps_rating,

//       "exterior_cond_cans_cover": this.exterior_cond_cans_coverInput.getInputValue(),
//       "exterior_cond_cans_cover_rating": this.state.exterior_cond_cans_cover_rating,

//       "exterior_cond_doors": this.exterior_cond_doorsInput.getInputValue(),
//       "exterior_cond_doors_rating": this.state.exterior_cond_doors_rating,

//       "exterior_cond_courtyard": this.exterior_cond_courtyardInput.getInputValue(),
//       "exterior_cond_courtyard_rating": this.state.exterior_cond_courtyard_rating,

//       "exterior_cond_yard_drain": this.exterior_cond_yard_drainInput.getInputValue(),
//       "exterior_cond_yard_drain_rating": this.state.exterior_cond_yard_drain_rating,

//       "exterior_cond_fountain_walls": this.exterior_cond_fountain_wallsInput.getInputValue(),
//       "exterior_cond_fountain_walls_rating": this.state.exterior_cond_fountain_walls_rating,

//       "exterior_cond_lights_at_entry": this.exterior_cond_lights_at_entryInput.getInputValue(),
//       "exterior_cond_lights_at_entry_rating": this.state.exterior_cond_lights_at_entry_rating,

//       "exterior_cond_lights_at_yard": this.exterior_cond_lights_at_yardInput.getInputValue(),
//       "exterior_cond_lights_at_yard_rating": this.state.exterior_cond_lights_at_yard_rating,

//       "entrance_entrance_door": this.entrance_entrance_doorInput.getInputValue(),
//       "entrance_entrance_door_rating": this.state.entrance_entrance_door_rating,

//       "entrance_sidelight": this.entrance_sidelightInput.getInputValue(),
//       "entrance_sidelight_rating": this.state.entrance_sidelight_rating,

//       "entrance_intercom": this.entrance_intercomInput.getInputValue(),
//       "entrance_intercom_rating": this.state.entrance_intercom_rating,

//       "entrance_mailboxes": this.entrance_mailboxesInput.getInputValue(),
//       "entrance_mailboxes_rating": this.state.entrance_mailboxes_rating,

//       "entrance_floor": this.entrance_floorInput.getInputValue(),
//       "entrance_floor_rating": this.state.entrance_floor_rating,

//       "entrance_walls": this.entrance_wallsInput.getInputValue(),
//       "entrance_walls_rating": this.state.entrance_walls_rating,

//       "entrance_lightning": this.entrance_lightningInput.getInputValue(),
//       "entrance_lightning_rating": this.state.entrance_lightning_rating,

//       "stairwells_steps_treads": this.stairwells_steps_treadsInput.getInputValue(),
//       "stairwells_steps_treads_rating": this.state.stairwells_steps_treads_rating,

//       "stairwells_walls": this.stairwells_wallsInput.getInputValue(),
//       "stairwells_walls_rating": this.state.stairwells_walls_rating,

//       "stairwells_lightning": this.stairwells_lightningInput.getInputValue(),
//       "stairwells_lightning_rating": this.state.stairwells_lightning_rating,

//       "stairwells_emergency_lightning": this.stairwells_emergency_lightningInput.getInputValue(),
//       "stairwells_emergency_lightning_rating": this.state.stairwells_emergency_lightning_rating,

//       "stairwells_doors": this.stairwells_doorsInput.getInputValue(),
//       "stairwells_doors_rating": this.state.stairwells_doors_rating,

//       "stairwells_windows": this.stairwells_windowsInput.getInputValue(),
//       "stairwells_windows_rating": this.state.stairwells_windows_rating,

//       "stairwells_signs": this.stairwells_signsInput.getInputValue(),
//       "stairwells_signs_rating": this.state.stairwells_signs_rating,

//       "emergency_exit_lightning": this.emergency_exit_lightningInput.getInputValue(),
//       "emergency_exit_lightning_rating": this.state.emergency_exit_lightning_rating,

//       "emergency_exit_floor": this.emergency_exit_floorInput.getInputValue(),
//       "emergency_exit_floor_rating": this.state.emergency_exit_floor_rating,

//       "emergency_exit_doors": this.emergency_exit_doorsInput.getInputValue(),
//       "emergency_exit_doors_rating": this.state.emergency_exit_doors_rating,

//       "emergency_exit_signs": this.emergency_exit_signsInput.getInputValue(),
//       "emergency_exit_signs_rating": this.state.emergency_exit_signs_rating,

//       "roof_bulkhead": this.roof_bulkheadInput.getInputValue(),
//       "roof_bulkhead_rating": this.state.roof_bulkhead_rating,

//       "roof_roof_doors": this.roof_roof_doorsInput.getInputValue(),
//       "roof_roof_doors_rating": this.state.roof_roof_doors_rating,

//       "roof_roof_surface": this.roof_roof_surfaceInput.getInputValue(),
//       "roof_roof_surface_rating": this.state.roof_roof_surface_rating,

//       "roof_gutter": this.roof_gutterInput.getInputValue(),
//       "roof_gutter_rating": this.state.roof_gutter_rating,

//       "roof__leaders": this.roof__leadersInput.getInputValue(),
//       "roof__leaders_rating": this.state.roof__leaders_rating,

//       "roof_skylight": this.roof_skylightInput.getInputValue(),
//       "roof_skylight_rating": this.state.roof_skylight_rating,

//       "roof_parapet": this.roof_parapetInput.getInputValue(),
//       "roof_parapet_rating": this.state.roof_parapet_rating,
//       "fire_escapes_ladders": this.fire_escapes_laddersInput.getInputValue(),
//       "fire_escapes_ladders_rating": this.state.fire_escapes_ladders_rating,

//       "fire_escapes_basket": this.fire_escapes_basketInput.getInputValue(),
//       "fire_escapes_basket_rating": this.state.fire_escapes_basket_rating,

//       "fire_escapes_railling": this.fire_escapes_raillingInput.getInputValue(),
//       "fire_escapes_railling_rating": this.state.fire_escapes_railling_rating,

//       "fire_escapes_ladder_shoes": this.fire_escapes_ladder_shoesInput.getInputValue(),
//       "fire_escapes_ladder_shoes_rating": this.state.fire_escapes_ladder_shoes_rating,

//       "lobbies_floor": this.lobbies_floorInput.getInputValue(),
//       "lobbies_floor_rating": this.state.lobbies_floor_rating,

//       "lobbies_walls": this.lobbies_wallsInput.getInputValue(),
//       "lobbies_walls_rating": this.state.lobbies_walls_rating,

//       "lobbies_lightning_emergency": this.lobbies_lightning_emergencyInput.getInputValue(),
//       "lobbies_lightning_emergency_rating": this.state.lobbies_lightning_emergency_rating,

//       "lobbies_windows_sills": this.lobbies_windows_sillsInput.getInputValue(),
//       "lobbies_windows_sills_rating": this.state.lobbies_windows_sills_rating,

//       "basement_doors": this.basement_doorsInput.getInputValue(),
//       "basement_doors_rating": this.state.basement_doors_rating,

//       "basement_stairs": this.basement_stairsInput.getInputValue(),
//       "basement_stairs_rating": this.state.basement_stairs_rating,

//       "basement_floor": this.basement_floorInput.getInputValue(),
//       "basement_floor_rating": this.state.basement_floor_rating,

//       "basement_walls": this.basement_wallsInput.getInputValue(),
//       "basement_walls_rating": this.state.basement_walls_rating,

//       "basement_lightning": this.basement_lightningInput.getInputValue(),
//       "basement_lightning_rating": this.state.basement_lightning_rating,

//       "basement_windows": this.basement_windowsInput.getInputValue(),
//       "basement_windows_rating": this.state.basement_windows_rating,

//       "basement_garbage_room": this.basement_garbage_roomInput.getInputValue(),
//       "basement_garbage_room_rating": this.state.basement_garbage_room_rating,

//       "laundry_room_equipment": this.laundry_room_equipmentInput.getInputValue(),
//       "laundry_room_equipment_rating": this.state.laundry_room_equipment_rating,

//       "laundry_room_floor": this.laundry_room_floorInput.getInputValue(),
//       "laundry_room_floor_rating": this.state.laundry_room_floor_rating,

//       "laundry_room_walls": this.laundry_room_wallsInput.getInputValue(),
//       "laundry_room_walls_rating": this.state.laundry_room_walls_rating,

//       "laundry_room_lightning": this.laundry_room_lightningInput.getInputValue(),
//       "laundry_room_lightning_rating": this.state.laundry_room_lightning_rating,

//       "boiler_fuel_supply": this.boiler_fuel_supplyInput.getInputValue(),
//       "boiler_fuel_supply_rating": this.state.boiler_fuel_supply_rating,

//       "boiler_gauge_color_coding_markings": this.boiler_gauge_color_coding_markingsInput.getInputValue(),
//       "boiler_gauge_color_coding_markings_rating": this.state.boiler_gauge_color_coding_markings_rating,


//       "boiler_nurners": this.boiler_nurnersInput.getInputValue(),
//       "boiler_nurners_rating": this.state.boiler_nurners_rating,

//       "boiler_coils": this.boiler_coilsInput.getInputValue(),
//       "boiler_coils_rating": this.state.boiler_coils_rating,

//       "boiler_oil_tank": this.boiler_oil_tankInput.getInputValue(),
//       "boiler_oil_tank_rating": this.state.boiler_oil_tank_rating,

//       "boiler_plumbing": this.boiler_plumbingInput.getInputValue(),
//       "boiler_plumbing_rating": this.state.boiler_plumbing_rating,

//       "boiler_leaks": this.boiler_leaksInput.getInputValue(),
//       "boiler_leaks_rating": this.state.boiler_leaks_rating,

//       "boiler_overheads_returns": this.boiler_overheads_returnsInput.getInputValue(),
//       "boiler_overheads_returns_rating": this.state.boiler_overheads_returns_rating,

//       "meters_lightning": this.meters_lightningInput.getInputValue(),
//       "meters_lightning_rating": this.state.meters_lightning_rating,

//       "meters_accessibility": this.meters_accessibilityInput.getInputValue(),
//       "meters_accessibility_rating": this.state.meters_accessibility_rating,

//       "meters_up_keeping": this.meters_up_keepingInput.getInputValue(),
//       "meters_up_keeping_rating": this.state.meters_up_keeping_rating,

//       "elevator_lightning": this.elevator_lightningInput.getInputValue(),
//       "elevator_lightning_rating": this.state.elevator_lightning_rating,

//       "elevator_signals": this.elevator_signalsInput.getInputValue(),
//       "elevator_signals_rating": this.state.elevator_signals_rating,

//       "elevator_doors": this.elevator_doorsInput.getInputValue(),
//       "elevator_doors_rating": this.state.elevator_doors_rating,

//       "elevator_cab_floor": this.elevator_cab_floorInput.getInputValue(),
//       "elevator_cab_floor_rating": this.state.elevator_cab_floor_rating,

//       "elevator_cab_walls": this.elevator_cab_wallsInput.getInputValue(),
//       "elevator_cab_walls_rating": this.state.elevator_cab_walls_rating,

//       "elevator_cab_ceilling": this.elevator_cab_ceillingInput.getInputValue(),
//       "elevator_cab_ceilling_rating": this.state.elevator_cab_ceilling_rating,

//       "elevator_floor_nmber_on_doors": this.elevator_floor_nmber_on_doorsInput.getInputValue(),
//       "elevator_floor_nmber_on_doors_rating": this.state.elevator_floor_nmber_on_doors_rating,

//       "fire_equipment_sprinklers": this.fire_equipment_sprinklersInput.getInputValue(),
//       "fire_equipment_sprinklers_rating": this.state.fire_equipment_sprinklers_rating,

//       "fire_equipment_extinguishers": this.fire_equipment_extinguishersInput.getInputValue(),
//       "fire_equipment_extinguishers_rating": this.state.fire_equipment_extinguishers_rating,

//       "fire_equipment_alarm_system": this.fire_equipment_alarm_systemInput.getInputValue(),
//       "fire_equipment_alarm_system_rating": this.state.fire_equipment_alarm_system_rating,


//       "vacant_units_apt1": this.vacant_units_apt1Input.getInputValue(),
//       "vacant_units_apt1_rating": this.state.vacant_units_apt1_rating,

//       "vacant_units_apt2": this.vacant_units_apt2Input.getInputValue(),
//       "vacant_units_apt2_rating": this.state.vacant_units_apt2_rating,

//       "inventory": this.state.inventory,
//       "inventory_rating": this.state.inventory_rating,

//       "problem": this.problemInput.getInputValue(),
//       "notes": this.notesInput.getInputValue(),

//     }


//     let a = []
//     Object.entries(body).forEach(function ([key, value]) {
//       a.push('<tr><td>' + `${key}` + '</td><td>' + `${value}` + '</td></tr>')
//     })


//     let options = {
//       // HTML Content for PDF.
//       // I am putting all the HTML code in Single line but if you want to use large HTML code then you can use + Symbol to add them.
//       //html: '<h1 style="text-align: center;"><strong>Welcome Guys</strong></h1><p style="text-align: center;">In This Tutorial we would learn about creating PDF File using HTML Text.</p><p style="text-align: center;"><strong>ReactNativeCode.com</strong></p>',
//       // Setting UP File Name for PDF File.

//       html: `<p>
//           <h1 style="color:red;"><u>Build Inspection Details</u></h1></p>
//           <table border=1> ${a} </table>`,

//       fileName: 'test',

//       //File directory in which the PDF File Will Store.
//       directory: 'Albums',
//     };

//     let file = await RNHTMLtoPDF.convert(options);

//     console.log(file.filePath);

//     Alert.alert(file.filePath);

//     this.setState({ filePath: file.filePath });
//   }

//   Call = () => {
//     // if (this.isValidate) {
//     /*----------------------------------------------------------- */
//     var formdata = new FormData();
//     formdata.append("building", this.buildingInput.getInputValue());
//     formdata.append("week_of", this.week_ofInput.getInputValue());
//     formdata.append("manager", this.managerInput.getInputValue());
//     formdata.append("supt", this.suptInput.getInputValue());
//     formdata.append("rating_choose_from", this.state.supt_rating);

//     formdata.append("exterior_cond_sidewalk_steps", this.exterior_cond_sidewalk_stepsInput.getInputValue());
//     formdata.append("exterior_cond_sidewalk_steps_rating", this.state.exterior_cond_sidewalk_steps_rating);

//     formdata.append("exterior_cond_cans_cover", this.exterior_cond_cans_coverInput.getInputValue());
//     formdata.append("exterior_cond_cans_cover_rating", this.state.exterior_cond_cans_cover_rating);

//     formdata.append("exterior_cond_doors", this.exterior_cond_doorsInput.getInputValue());
//     formdata.append("exterior_cond_doors_rating", this.state.exterior_cond_doors_rating);

//     formdata.append("exterior_cond_courtyard", this.exterior_cond_courtyardInput.getInputValue());
//     formdata.append("exterior_cond_courtyard_rating", this.state.exterior_cond_courtyard_rating);

//     formdata.append("exterior_cond_yard_drain", this.exterior_cond_yard_drainInput.getInputValue());
//     formdata.append("exterior_cond_yard_drain_rating", this.state.exterior_cond_yard_drain_rating);

//     formdata.append("exterior_cond_fountain_walls", this.exterior_cond_fountain_wallsInput.getInputValue());
//     formdata.append("exterior_cond_fountain_walls_rating", this.state.exterior_cond_fountain_walls_rating);

//     formdata.append("exterior_cond_lights_at_entry", this.exterior_cond_lights_at_entryInput.getInputValue());
//     formdata.append("exterior_cond_lights_at_entry_rating", this.state.exterior_cond_lights_at_entry_rating);

//     formdata.append("exterior_cond_lights_at_yard", this.exterior_cond_lights_at_yardInput.getInputValue());
//     formdata.append("exterior_cond_lights_at_yard_rating", this.state.exterior_cond_lights_at_yard_rating);

//     formdata.append("entrance_entrance_door", this.entrance_entrance_doorInput.getInputValue());
//     formdata.append("entrance_entrance_door_rating", this.state.entrance_entrance_door_rating);

//     formdata.append("entrance_sidelight", this.entrance_sidelightInput.getInputValue());
//     formdata.append("entrance_sidelight_rating", this.state.entrance_sidelight_rating);

//     formdata.append("entrance_intercom", this.entrance_intercomInput.getInputValue());
//     formdata.append("entrance_intercom_rating", this.state.entrance_intercom_rating);

//     formdata.append("entrance_mailboxes", this.entrance_mailboxesInput.getInputValue());
//     formdata.append("entrance_mailboxes_rating", this.state.entrance_mailboxes_rating);

//     formdata.append("entrance_floor", this.entrance_floorInput.getInputValue());
//     formdata.append("entrance_floor_rating", this.state.entrance_floor_rating);

//     formdata.append("entrance_walls", this.entrance_wallsInput.getInputValue());
//     formdata.append("entrance_walls_rating", this.state.entrance_walls_rating);

//     formdata.append("entrance_lightning", this.entrance_lightningInput.getInputValue());
//     formdata.append("entrance_lightning_rating", this.state.entrance_lightning_rating);

//     formdata.append("stairwells_steps_treads", this.stairwells_steps_treadsInput.getInputValue());
//     formdata.append("stairwells_steps_treads_rating", this.state.stairwells_steps_treads_rating);

//     formdata.append("stairwells_walls", this.stairwells_wallsInput.getInputValue());
//     formdata.append("stairwells_walls_rating", this.state.stairwells_walls_rating);

//     formdata.append("stairwells_lightning", this.stairwells_lightningInput.getInputValue());
//     formdata.append("stairwells_lightning_rating", this.state.stairwells_lightning_rating);

//     formdata.append("stairwells_emergency_lightning", this.stairwells_emergency_lightningInput.getInputValue());
//     formdata.append("stairwells_emergency_lightning_rating", this.state.stairwells_emergency_lightning_rating);

//     formdata.append("stairwells_doors", this.stairwells_doorsInput.getInputValue());
//     formdata.append("stairwells_doors_rating", this.state.stairwells_doors_rating);

//     formdata.append("stairwells_windows", this.stairwells_windowsInput.getInputValue());
//     formdata.append("stairwells_windows_rating", this.state.stairwells_windows_rating);

//     formdata.append("stairwells_signs", this.stairwells_signsInput.getInputValue());
//     formdata.append("stairwells_signs_rating", this.state.stairwells_signs_rating);

//     formdata.append("emergency_exit_lightning", this.emergency_exit_lightningInput.getInputValue());
//     formdata.append("emergency_exit_lightning_rating", this.state.emergency_exit_lightning_rating);

//     formdata.append("emergency_exit_floor", this.emergency_exit_floorInput.getInputValue());
//     formdata.append("emergency_exit_floor_rating", this.state.emergency_exit_floor_rating);

//     formdata.append("emergency_exit_doors", this.emergency_exit_doorsInput.getInputValue());
//     formdata.append("emergency_exit_doors_rating", this.state.emergency_exit_doors_rating);

//     formdata.append("emergency_exit_signs", this.emergency_exit_signsInput.getInputValue());
//     formdata.append("emergency_exit_signs_rating", this.state.emergency_exit_signs_rating);

//     formdata.append("roof_bulkhead", this.roof_bulkheadInput.getInputValue());
//     formdata.append("roof_bulkhead_rating", this.state.roof_bulkhead_rating);

//     formdata.append("roof_roof_doors", this.roof_roof_doorsInput.getInputValue());
//     formdata.append("roof_roof_doors_rating", this.state.roof_roof_doors_rating);

//     formdata.append("roof_roof_surface", this.roof_roof_surfaceInput.getInputValue());
//     formdata.append("roof_roof_surface_rating", this.state.roof_roof_surface_rating);

//     formdata.append("roof_gutter", this.roof_gutterInput.getInputValue());
//     formdata.append("roof_gutter_rating", this.state.roof_gutter_rating);

//     formdata.append("roof__leaders", this.roof__leadersInput.getInputValue());
//     formdata.append("roof__leaders_rating", this.state.roof__leaders_rating);

//     formdata.append("roof_skylight", this.roof_skylightInput.getInputValue());
//     formdata.append("roof_skylight_rating", this.state.roof_skylight_rating);

//     formdata.append("roof_parapet", this.roof_parapetInput.getInputValue());
//     formdata.append("roof_parapet_rating", this.state.roof_parapet_rating);

//     formdata.append("fire_escapes_ladders", this.fire_escapes_laddersInput.getInputValue());
//     formdata.append("fire_escapes_ladders_rating", this.state.fire_escapes_ladders_rating);

//     formdata.append("fire_escapes_basket", this.fire_escapes_basketInput.getInputValue());
//     formdata.append("fire_escapes_basket_rating", this.state.fire_escapes_basket_rating);

//     formdata.append("fire_escapes_railling", this.fire_escapes_raillingInput.getInputValue());
//     formdata.append("fire_escapes_railling_rating", this.state.fire_escapes_railling_rating);

//     formdata.append("fire_escapes_ladder_shoes", this.fire_escapes_ladder_shoesInput.getInputValue());
//     formdata.append("fire_escapes_ladder_shoes_rating", this.state.fire_escapes_ladder_shoes_rating);

//     formdata.append("lobbies_floor", this.lobbies_floorInput.getInputValue());
//     formdata.append("lobbies_floor_rating", this.state.lobbies_floor_rating);

//     formdata.append("lobbies_walls", this.lobbies_wallsInput.getInputValue());
//     formdata.append("lobbies_walls_rating", this.state.lobbies_walls_rating);

//     formdata.append("lobbies_lightning_emergency", this.lobbies_lightning_emergencyInput.getInputValue());
//     formdata.append("lobbies_lightning_emergency_rating", this.state.lobbies_lightning_emergency_rating);

//     formdata.append("lobbies_windows_sills", this.lobbies_windows_sillsInput.getInputValue());
//     formdata.append("lobbies_windows_sills_rating", this.state.lobbies_windows_sills_rating);

//     formdata.append("basement_doors", this.basement_doorsInput.getInputValue());
//     formdata.append("basement_doors_rating", this.state.basement_doors_rating);

//     formdata.append("basement_stairs", this.basement_stairsInput.getInputValue());
//     formdata.append("basement_stairs_rating", this.state.basement_stairs_rating);

//     formdata.append("basement_floor", this.basement_floorInput.getInputValue());
//     formdata.append("basement_floor_rating", this.state.basement_floor_rating);

//     formdata.append("basement_walls", this.basement_wallsInput.getInputValue());
//     formdata.append("basement_walls_rating", this.state.basement_walls_rating);

//     formdata.append("basement_lightning", this.basement_lightningInput.getInputValue());
//     formdata.append("basement_lightning_rating", this.state.basement_lightning_rating);

//     formdata.append("basement_windows", this.basement_windowsInput.getInputValue());
//     formdata.append("basement_windows_rating", this.state.basement_windows_rating);

//     formdata.append("basement_garbage_room", this.basement_garbage_roomInput.getInputValue());
//     formdata.append("basement_garbage_room_rating", this.state.basement_garbage_room_rating);

//     formdata.append("laundry_room_equipment", this.laundry_room_equipmentInput.getInputValue());
//     formdata.append("laundry_room_equipment_rating", this.state.laundry_room_equipment_rating);

//     formdata.append("laundry_room_floor", this.laundry_room_floorInput.getInputValue());
//     formdata.append("laundry_room_floor_rating", this.state.laundry_room_floor_rating);

//     formdata.append("laundry_room_walls", this.laundry_room_wallsInput.getInputValue());
//     formdata.append("laundry_room_walls_rating", this.state.laundry_room_walls_rating);

//     formdata.append("laundry_room_lightning", this.laundry_room_lightningInput.getInputValue());
//     formdata.append("laundry_room_lightning_rating", this.state.laundry_room_lightning_rating);

//     formdata.append("boiler_fuel_supply", this.boiler_fuel_supplyInput.getInputValue());
//     formdata.append("boiler_fuel_supply_rating", this.state.boiler_fuel_supply_rating);

//     formdata.append("boiler_gauge_color_coding_markings", this.boiler_gauge_color_coding_markingsInput.getInputValue());
//     formdata.append("boiler_gauge_color_coding_markings_rating", this.state.boiler_gauge_color_coding_markings_rating);


//     formdata.append("boiler_nurners", this.boiler_nurnersInput.getInputValue());
//     formdata.append("boiler_nurners_rating", this.state.boiler_nurners_rating);

//     formdata.append("boiler_coils", this.boiler_coilsInput.getInputValue());
//     formdata.append("boiler_coils_rating", this.state.boiler_coils_rating);

//     formdata.append("boiler_oil_tank", this.boiler_oil_tankInput.getInputValue());
//     formdata.append("boiler_oil_tank_rating", this.state.boiler_oil_tank_rating);

//     formdata.append("boiler_plumbing", this.boiler_plumbingInput.getInputValue());
//     formdata.append("boiler_plumbing_rating", this.state.boiler_plumbing_rating);

//     formdata.append("boiler_leaks", this.boiler_leaksInput.getInputValue());
//     formdata.append("boiler_leaks_rating", this.state.boiler_leaks_rating);

//     formdata.append("boiler_overheads_returns", this.boiler_overheads_returnsInput.getInputValue());
//     formdata.append("boiler_overheads_returns_rating", this.state.boiler_overheads_returns_rating);

//     formdata.append("meters_lightning", this.meters_lightningInput.getInputValue());
//     formdata.append("meters_lightning_rating", this.state.meters_lightning_rating);

//     formdata.append("meters_accessibility", this.meters_accessibilityInput.getInputValue());
//     formdata.append("meters_accessibility_rating", this.state.meters_accessibility_rating);

//     formdata.append("meters_up_keeping", this.meters_up_keepingInput.getInputValue());
//     formdata.append("meters_up_keeping_rating", this.state.meters_up_keeping_rating);

//     formdata.append("elevator_lightning", this.elevator_lightningInput.getInputValue());
//     formdata.append("elevator_lightning_rating", this.state.elevator_lightning_rating);

//     formdata.append("elevator_signals", this.elevator_signalsInput.getInputValue());
//     formdata.append("elevator_signals_rating", this.state.elevator_signals_rating);

//     formdata.append("elevator_doors", this.elevator_doorsInput.getInputValue());
//     formdata.append("elevator_doors_rating", this.state.elevator_doors_rating);

//     formdata.append("elevator_cab_floor", this.elevator_cab_floorInput.getInputValue());
//     formdata.append("elevator_cab_floor_rating", this.state.elevator_cab_floor_rating);

//     formdata.append("elevator_cab_walls", this.elevator_cab_wallsInput.getInputValue());
//     formdata.append("elevator_cab_walls_rating", this.state.elevator_cab_walls_rating);

//     formdata.append("elevator_cab_ceilling", this.elevator_cab_ceillingInput.getInputValue());
//     formdata.append("elevator_cab_ceilling_rating", this.state.elevator_cab_ceilling_rating);

//     formdata.append("elevator_floor_nmber_on_doors", this.elevator_floor_nmber_on_doorsInput.getInputValue());
//     formdata.append("elevator_floor_nmber_on_doors_rating", this.state.elevator_floor_nmber_on_doors_rating);

//     formdata.append("fire_equipment_sprinklers", this.fire_equipment_sprinklersInput.getInputValue());
//     formdata.append("fire_equipment_sprinklers_rating", this.state.fire_equipment_sprinklers_rating);

//     formdata.append("fire_equipment_extinguishers", this.fire_equipment_extinguishersInput.getInputValue());
//     formdata.append("fire_equipment_extinguishers_rating", this.state.fire_equipment_extinguishers_rating);

//     formdata.append("fire_equipment_alarm_system", this.fire_equipment_alarm_systemInput.getInputValue());
//     formdata.append("fire_equipment_alarm_system_rating", this.state.fire_equipment_alarm_system_rating);


//     formdata.append("vacant_units_apt1", this.vacant_units_apt1Input.getInputValue());
//     formdata.append("vacant_units_apt1_rating", this.state.vacant_units_apt1_rating);

//     formdata.append("vacant_units_apt2", this.vacant_units_apt2Input.getInputValue());
//     formdata.append("vacant_units_apt2_rating", this.state.vacant_units_apt2_rating);

//     formdata.append("inventory", this.inventoryInput.getInputValue());
//     formdata.append("inventory_rating", this.state.inventory_rating);

//     formdata.append("problem", this.problemInput.getInputValue());
//     formdata.append("notes", this.notesInput.getInputValue());


//     var requestOptions = {
//       method: 'POST',
//       body: formdata,
//       redirect: 'follow'
//     };

//     this.setState({ pdf_data: formdata })
//     this.setState({ loading: true })


//     fetch("http://webmobril.org/dev/inspectionapp/api/inspection", requestOptions)
//       .then(response => response.json())
//       .then((response) => {
//         this.setState({ loading: false })
//         console.log("inspection response=-=+++++=->>", response)
//         // this.textInput.clear()
//         this.setState({
         
//           pdf_data: [],
//           selected: '',
//           value: '',
//           /*------------------------ Supt -------------------------------------------------- */
//           supt_rating: '',
//           /* ------------------------- Exterior ------------------------------------------ */
//           exterior_cond_sidewalk_steps_rating: '',
//           exterior_cond_cans_cover_rating: '',
//           exterior_cond_doors_rating: '',
//           exterior_cond_courtyard_rating: '',
//           exterior_cond_yard_drain_rating: '',
//           exterior_cond_fountain_walls_rating: '',
//           exterior_cond_lights_at_entry_rating: '',
//           exterior_cond_lights_at_yard_rating: '',
//           /*---------------------Entrance --------------------------------------------- */
//          entrance_entrance_door_rating: '',
//           entrance_sidelight_rating: '',
//           entrance_intercom_rating: '',
//           entrance_mailboxes_rating: '',
//           entrance_floor_rating: '',
//           entrance_walls_rating: '',
//           entrance_lightning_rating: '',
    
//           /*-----------------Stairwells------------------------ */
//           stairwells_steps_treads_rating: '',
//           stairwells_walls_rating: '',
//           stairwells_lightning_rating: '',
//          stairwells_emergency_lightning_rating: '',
//          stairwells_doors_rating: '',
//           stairwells_windows_rating: '',
//            stairwells_signs_rating: '',
    
//           /*----------------------Emergency------------------- */
          
//           emergency_exit_lightning_rating: '',
//           emergency_exit_floor_rating: '',
//           emergency_exit_doors_rating: '',
//           emergency_exit_signs_rating: '',
    
//           /*--------------------Roof ---------------------------- */
         
//           roof_bulkhead_rating: '',
//           roof_roof_doors_rating: '',
//           roof_roof_surface_rating: '',
//           roof_gutter_rating: '',
//           roof__leaders_rating: '',
//           roof_skylight_rating: '',
//           roof_parapet_rating: '',
    
//           /*------------------------------ fire escapes ----------------------------------- */
         
//           fire_escapes_ladders_rating: '',
//           fire_escapes_basket_rating: '',
//           fire_escapes_railling_rating: '',
//           fire_escapes_ladder_shoes_rating: '',
    
//           /*----------------------------- Lobbies -------------------------------------------- */
    
//           lobbies_floor_rating: '',
//           lobbies_walls_rating: '',
//           lobbies_lightning_emergency_rating: '',
//           lobbies_windows_sills_rating: '',
    
//           /* --------------------------------- basement ----------------------------------------------- */
         
//           basement_doors_rating: '',
//           basement_stairs_rating: '',
//           basement_floor_rating: '',
//           basement_walls_rating: '',
//           basement_lightning_rating: '',
//           basement_windows_rating: '',
//           basement_garbage_room_rating: '',
    
//           /*--------------------------------------- Laundry Room --------------------------------------- */
    
//           laundry_room_equipment_rating: '',
//           laundry_room_floor_rating: '',
//           laundry_room_walls_rating: '',
//           laundry_room_lightning_rating: '',
    
//           /*------------------------------------ Boiler------------------------------------------------- */
    
//           boiler_fuel_supply_rating: '',
//           boiler_gauge_color_coding_markings_rating: '',
//           boiler_nurners_rating: '',
//           boiler_coils_rating: '',
//           boiler_oil_tank_rating: '',
//           boiler_plumbing_rating: '',
//           boiler_leaks_rating: '',
//           boiler_overheads_returns_rating: '',
    
//           /*------------------------------------ Meters ------------------------------------------------------- */
    
//           meters_lightning_rating: '',
//           meters_accessibility_rating: '',
//           meters_up_keeping_rating: '',
    
//           /*--------------------------- Elevator ------------------------------------------------------ */
    
    
//           elevator_lightning_rating: '',
//           elevator_signals_rating: '',
//           elevator_doors_rating: '',
//           elevator_cab_floor_rating: '',
//           elevator_cab_walls_rating: '',
//           elevator_cab_ceilling_rating: '',
//           elevator_floor_nmber_on_doors_rating: '',
    
//           /* -------------------------------------------------Fire Equipments --------------------------------------- */
         
//           fire_equipment_sprinklers_rating: '',
//           fire_equipment_extinguishers_rating: '',
//           fire_equipment_alarm_system_rating: '',
    
//           /*-------------------------------------- Vacant---------------------------------------- */
//           vacant_units_apt1_rating: '',
//           vacant_units_apt2_rating: '',
//           inventory_rating: '',
    
//           selectedChooseRating: '',
    
//         })
// this.buildingInput.setVal('')
// this.week_ofInput.setVal('')
// this.managerInput.setVal('')

// this.suptInput.setVal('')
// this.exterior_cond_sidewalk_stepsInput.setVal('')
// this.exterior_cond_cans_coverInput.setVal('')
// this.exterior_cond_doorsInput.setVal('')
// this.exterior_cond_courtyardInput.setVal('')
// this.exterior_cond_yard_drainInput.setVal('')

// this.exterior_cond_fountain_wallsInput.setVal('')
// this.exterior_cond_lights_at_entryInput.setVal('')
// this.exterior_cond_lights_at_yardInput.setVal('')
// this.entrance_entrance_doorInput.setVal('')
// this.entrance_sidelightInput.setVal('')
// this.entrance_intercomInput.setVal('')

// this.entrance_mailboxesInput.setVal('')
// this.entrance_floorInput.setVal('')
// this.entrance_wallsInput.setVal('')
// this.entrance_lightningInput.setVal('')
// this.stairwells_steps_treadsInput.setVal('')
// this.stairwells_wallsInput.setVal('')

// this.stairwells_lightningInput.setVal('')
// this.stairwells_emergency_lightningInput.setVal('')
// this.stairwells_doorsInput.setVal('')
// this.stairwells_windowsInput.setVal('')
// this.stairwells_signsInput.setVal('')
// this.emergency_exit_lightningInput.setVal('')

// this.emergency_exit_floorInput.setVal('')
// this.emergency_exit_doorsInput.setVal('')
// this.emergency_exit_signsInput.setVal('')
// this.roof_bulkheadInput.setVal('')
// this.roof_roof_doorsInput.setVal('')
// this.roof_roof_surfaceInput.setVal('')

// this.roof_gutterInput.setVal('')
// this.roof__leadersInput.setVal('')
// this.roof_skylightInput.setVal('')
// this.roof_parapetInput.setVal('')
// this.fire_escapes_laddersInput.setVal('')
// this.fire_escapes_basketInput.setVal('')

// this.fire_escapes_raillingInput.setVal('')
// this.fire_escapes_ladder_shoesInput.setVal('')
// this.lobbies_floorInput.setVal('')
// this.lobbies_wallsInput.setVal('')
// this.lobbies_lightning_emergencyInput.setVal('')
// this.lobbies_windows_sillsInput.setVal('')

// this.basement_doorsInput.setVal('')
// this.basement_stairsInput.setVal('')
// this.basement_floorInput.setVal('')
// this.basement_wallsInput.setVal('')
// this.basement_lightningInput.setVal('')
// this.basement_windowsInput.setVal('')

// this.basement_garbage_roomInput.setVal('')
// this.laundry_room_equipmentInput.setVal('')
// this.laundry_room_floorInput.setVal('')
// this.laundry_room_wallsInput.setVal('')
// this.laundry_room_lightningInput.setVal('')
// this.boiler_fuel_supplyInput.setVal('')

// this.boiler_gauge_color_coding_markingsInput.setVal('')
// this.boiler_nurnersInput.setVal('')
// this.boiler_coilsInput.setVal('')
// this.boiler_oil_tankInput.setVal('')
// this.boiler_plumbingInput.setVal('')
// this.boiler_leaksInput.setVal('')

// this.boiler_overheads_returnsInput.setVal('')
// this.meters_lightningInput.setVal('')
// this.meters_accessibilityInput.setVal('')
// this.meters_up_keepingInput.setVal('')
// this.elevator_lightningInput.setVal('')
// this.elevator_signalsInput.setVal('')

// this.elevator_doorsInput.setVal('')
// this.elevator_cab_floorInput.setVal('')
// this.elevator_cab_wallsInput.setVal('')
// this.elevator_cab_ceillingInput.setVal('')
// this.elevator_floor_nmber_on_doorsInput.setVal('')
// this.fire_equipment_sprinklersInput.setVal('')

// this.fire_equipment_extinguishersInput.setVal('')
// this.fire_equipment_alarm_systemInput.setVal('')
// this.vacant_units_apt1Input.setVal('')
// this.vacant_units_apt2Input.setVal('')
// this.inventoryInput.setVal('')
// this.problemInput.setVal('')
// this.notesInput.setVal('')
//       })
//       .catch(error => {
//         this.setState({ loading: false })
//       });
//   }
 
//   isValidate = () => {
   
//     if (!validateFields(this.buildingInput.getInputValue(), 'Building field')) {
//       return false
//     }
//     else if (!validateDate(this.week_ofInput.getInputValue(), 'Date field')) {
//       return false
//     }
//     else if (!validateFields(this.managerInput.getInputValue(), 'Manager field')) {
//       return false
//     }
//     else if (!validateFields(this.suptInput.getInputValue(), 'Supt Field')) {
//       return false
//     }
//     else if (!validateFields(this.state.supt_rating, 'Supt rating')) {
//       return false
//     }
//     else if (!validateFields(this.exterior_cond_sidewalk_stepsInput.getInputValue(), 'exterior_cond_sidewalk_steps field')) {
//       return false
//     }
//     else if (!validateFields(this.state.exterior_cond_sidewalk_steps_rating, 'exterior_cond_sidewalk_steps_rating')) {
//       return false
//     }

//     else if (!validateFields(this.exterior_cond_cans_coverInput.getInputValue(), 'exterior_cond_cans_cover field')) {
//       return false
//     }
//     else if (!validateFields(this.state.exterior_cond_cans_cover_rating, 'exterior_cond_cans_cover_rating')) {
//       return false
//     }

//     else if (!validateFields(this.exterior_cond_doorsInput.getInputValue(), 'exterior_cond_doors field')) {
//       return false
//     }
//     else if (!validateFields(this.state.exterior_cond_doors_rating, 'exterior_cond_doors_rating')) {
//       return false
//     }

//     else if (!validateFields(this.exterior_cond_courtyardInput.getInputValue(), 'exterior_cond_courtyard field')) {
//       return false
//     }
//     else if (!validateFields(this.state.exterior_cond_courtyard_rating, 'exterior_cond_courtyard_rating')) {
//       return false
//     }

//     else if (!validateFields(this.exterior_cond_yard_drainInput.getInputValue(), 'exterior_cond_yard_drain field')) {
//       return false
//     }
//     else if (!validateFields(this.state.exterior_cond_yard_drain_rating, 'exterior_cond_yard_drain_rating')) {
//       return false
//     }

//     else if (!validateFields(this.exterior_cond_fountain_wallsInput.getInputValue(), 'exterior_cond_fountain_walls field')) {
//       return false
//     }
//     else if (!validateFields(this.state.exterior_cond_fountain_walls_rating, 'exterior_cond_fountain_walls_rating')) {
//       return false
//     }

//     else if (!validateFields(this.exterior_cond_lights_at_entryInput.getInputValue(), 'exterior_cond_lights_at_entry field')) {
//       return false
//     }
//     else if (!validateFields(this.state.exterior_cond_lights_at_entry_rating, 'exterior_cond_lights_at_entry_rating')) {
//       return false
//     }

//     else if (!validateFields(this.exterior_cond_lights_at_yardInput.getInputValue(), 'exterior_cond_lights_at_yard field')) {
//       return false
//     }
//     else if (!validateFields(this.state.exterior_cond_lights_at_yard_rating, 'exterior_cond_lights_at_yard_rating')) {
//       return false
//     }

//     else if (!validateFields(this.entrance_entrance_doorInput.getInputValue(), 'entrance_entrance_door field')) {
//       return false
//     }
//     else if (!validateFields(this.state.entrance_entrance_door_rating, 'entrance_entrance_door_rating')) {
//       return false
//     }

//     else if (!validateFields(this.entrance_sidelightInput.getInputValue(), 'entrance_sidelight field')) {
//       return false
//     }
//     else if (!validateFields(this.state.entrance_sidelight_rating, 'entrance_sidelight_rating')) {
//       return false
//     }



//     else if (!validateFields(this.entrance_intercomInput.getInputValue(), 'entrance_intercom field')) {
//       return false
//     }
//     else if (!validateFields(this.state.entrance_intercom_rating, 'entrance_intercom_rating')) {
//       return false
//     }
//     else if (!validateFields(this.entrance_mailboxesInput.getInputValue(), 'entrance_mailboxes field')) {
//       return false
//     }
//     else if (!validateFields(this.state.entrance_mailboxes_rating, 'entrance_mailboxes_rating')) {
//       return false
//     }

//     else if (!validateFields(this.entrance_floorInput.getInputValue(), 'entrance_floor field')) {
//       return false
//     }
//     else if (!validateFields(this.state.entrance_floor_rating, 'entrance_floor_rating')) {
//       return false
//     }
//     else if (!validateFields(this.entrance_wallsInput.getInputValue(), 'entrance_walls field')) {
//       return false
//     }
//     else if (!validateFields(this.state.entrance_walls_rating, 'entrance_walls_rating')) {
//       return false
//     }
//     else if (!validateFields(this.entrance_lightningInput.getInputValue(), 'entrance_lightning field')) {
//       return false
//     }
//     else if (!validateFields(this.state.entrance_lightning_rating, 'entrance_lightning_rating')) {
//       return false
//     }
//     else if (!validateFields(this.stairwells_steps_treadsInput.getInputValue(), 'stairwells_steps_treads field')) {
//       return false
//     }
//     else if (!validateFields(this.state.stairwells_steps_treads_rating, 'stairwells_steps_treads_rating')) {
//       return false
//     }
//     else if (!validateFields(this.stairwells_wallsInput.getInputValue(), 'stairwells_walls field')) {
//       return false
//     }
//     else if (!validateFields(this.state.stairwells_walls_rating, 'stairwells_walls_rating_rating')) {
//       return false
//     }
//     else if (!validateFields(this.stairwells_lightningInput.getInputValue(), 'stairwells_lightning field')) {
//       return false
//     }
//     else if (!validateFields(this.state.stairwells_lightning_rating, 'stairwells_lightning_rating')) {
//       return false
//     }
//     else if (!validateFields(this.stairwells_emergency_lightningInput.getInputValue(), 'stairwells_emergency_lightning field')) {
//       return false
//     }
//     else if (!validateFields(this.state.stairwells_emergency_lightning_rating, 'stairwells_emergency_lightning_rating')) {
//       return false
//     }

//     else if (!validateFields(this.stairwells_doorsInput.getInputValue(), 'stairwells_doors field')) {
//       return false
//     }
//     else if (!validateFields(this.state.stairwells_doors_rating, 'stairwells_doors_rating')) {
//       return false
//     }


//     else if (!validateFields(this.stairwells_windowsInput.getInputValue(), 'stairwells_windows field')) {
//       return false
//     }
//     else if (!validateFields(this.state.stairwells_windows_rating, 'stairwells_windows_rating')) {
//       return false
//     }

//     else if (!validateFields(this.stairwells_signsInput.getInputValue(), 'stairwells_signs field')) {
//       return false
//     }
//     else if (!validateFields(this.state.stairwells_signs_rating, 'stairwells_signs_rating')) {
//       return false
//     }


//     else if (!validateFields(this.emergency_exit_lightningInput.getInputValue(), 'emergency_exit_lightning field')) {
//       return false
//     } else if (!validateFields(this.state.emergency_exit_lightning_rating, 'emergency_exit_lightning_rating')) {
//       return false
//     }

//     else if (!validateFields(this.emergency_exit_floorInput.getInputValue(), 'emergency_exit_floor field')) {
//       return false
//     } else if (!validateFields(this.state.emergency_exit_floor_rating, 'emergency_exit_floor_rating')) {
//       return false
//     }


//     else if (!validateFields(this.emergency_exit_doorsInput.getInputValue(), 'emergency_exit_doors field')) {
//       return false
//     } else if (!validateFields(this.state.emergency_exit_doors_rating, 'emergency_exit_doors_rating')) {
//       return false
//     }

//     else if (!validateFields(this.emergency_exit_signsInput.getInputValue(), 'emergency_exit_signs field')) {
//       return false
//     } else if (!validateFields(this.state.emergency_exit_signs_rating, 'emergency_exit_signs_rating')) {
//       return false
//     }

//     else if (!validateFields(this.roof_bulkheadInput.getInputValue(), 'roof_bulkhead field')) {
//       return false
//     } else if (!validateFields(this.state.roof_bulkhead_rating, 'roof_bulkhead_rating')) {
//       return false
//     }


//     else if (!validateFields(this.roof_roof_doorsInput.getInputValue(), 'roof_roof_doors field')) {
//       return false
//     } else if (!validateFields(this.state.roof_roof_doors_rating, 'roof_roof_doors_rating')) {
//       return false
//     }


//     else if (!validateFields(this.roof_roof_surfaceInput.getInputValue(), 'roof_roof_surface field')) {
//       return false
//     } else if (!validateFields(this.state.roof_roof_surface_rating, 'roof_roof_surface_rating')) {
//       return false
//     }

//     else if (!validateFields(this.roof_gutterInput.getInputValue(), 'roof_gutter field')) {
//       return false
//     } else if (!validateFields(this.state.roof_gutter_rating, 'roof_gutter_rating')) {
//       return false
//     }


//     else if (!validateFields(this.roof__leadersInput.getInputValue(), 'roof__leaders field')) {
//       return false
//     } else if (!validateFields(this.state.roof__leaders_rating, 'roof__leaders_rating')) {
//       return false
//     }


//     else if (!validateFields(this.roof_skylightInput.getInputValue(), 'roof_skylight field')) {
//       return false
//     } else if (!validateFields(this.state.roof_skylight_rating, 'roof_skylight_rating')) {
//       return false
//     }

//     else if (!validateFields(this.roof_parapetInput.getInputValue(), 'roof_parapet field')) {
//       return false
//     } else if (!validateFields(this.state.roof_parapet_rating, 'roof_parapet_rating')) {
//       return false
//     }


//     else if (!validateFields(this.fire_escapes_laddersInput.getInputValue(), 'fire_escapes_ladders field')) {
//       return false
//     } else if (!validateFields(this.state.fire_escapes_ladders_rating, 'fire_escapes_ladders_rating')) {
//       return false
//     }


//     else if (!validateFields(this.fire_escapes_basketInput.getInputValue(), 'fire_escapes_basket field')) {
//       return false
//     } else if (!validateFields(this.state.fire_escapes_basket_rating, 'fire_escapes_basket_rating')) {
//       return false
//     }

//     else if (!validateFields(this.fire_escapes_raillingInput.getInputValue(), 'fire_escapes_railling field')) {
//       return false
//     } else if (!validateFields(this.state.fire_escapes_railling_rating, 'fire_escapes_railling_rating')) {
//       return false
//     }


//     else if (!validateFields(this.fire_escapes_ladder_shoesInput.getInputValue(), 'fire_escapes_ladder_shoes field')) {
//       return false
//     } else if (!validateFields(this.state.fire_escapes_ladder_shoes_rating, 'fire_escapes_ladder_shoes_rating')) {
//       return false
//     }

//     else if (!validateFields(this.lobbies_floorInput.getInputValue(), 'lobbies_floor field')) {
//       return false
//     } else if (!validateFields(this.state.lobbies_floor_rating, 'lobbies_floor_rating')) {
//       return false
//     }


//     else if (!validateFields(this.lobbies_wallsInput.getInputValue(), 'lobbies_walls field')) {
//       return false
//     } else if (!validateFields(this.state.lobbies_walls_rating, 'lobbies_walls_rating')) {
//       return false
//     }

//     else if (!validateFields(this.lobbies_lightning_emergencyInput.getInputValue(), 'lobbies_lightning_emergency field')) {
//       return false
//     } else if (!validateFields(this.state.lobbies_lightning_emergency_rating, 'lobbies_lightning_emergency_rating')) {
//       return false
//     }


//     else if (!validateFields(this.lobbies_windows_sillsInput.getInputValue(), 'lobbies_windows_sills field')) {
//       return false
//     }
//     else if (!validateFields(this.state.lobbies_windows_sills_rating, 'lobbies_windows_sills_rating')) {
//       return false
//     }


//     else if (!validateFields(this.basement_doorsInput.getInputValue(), 'basement_doors field')) {
//       return false
//     } else if (!validateFields(this.state.basement_doors_rating, 'basement_doors_rating')) {
//       return false
//     }


//     else if (!validateFields(this.basement_stairsInput.getInputValue(), 'basement_stairs field')) {
//       return false
//     } else if (!validateFields(this.state.basement_stairs_rating, 'basement_stairs')) {
//       return false
//     }


//     else if (!validateFields(this.basement_floorInput.getInputValue(), 'basement_floor field')) {
//       return false
//     } else if (!validateFields(this.state.basement_floor_rating, 'basement_floor_rating')) {
//       return false
//     }


//     else if (!validateFields(this.basement_wallsInput.getInputValue(), 'basement_walls field')) {
//       return false
//     } else if (!validateFields(this.state.basement_walls_rating, 'basement_walls_rating')) {
//       return false
//     }


//     else if (!validateFields(this.basement_lightningInput.getInputValue(), 'basement_lightning field')) {
//       return false
//     } else if (!validateFields(this.state.basement_lightning_rating, 'basement_lightning_rating')) {
//       return false
//     }

//     else if (!validateFields(this.basement_windowsInput.getInputValue(), 'basement_windows field')) {
//       return false
//     } else if (!validateFields(this.state.basement_windows_rating, 'basement_windows_rating')) {
//       return false
//     }


//     else if (!validateFields(this.basement_garbage_roomInput.getInputValue(), 'basement_garbage_room field')) {
//       return false
//     } else if (!validateFields(this.state.basement_garbage_room_rating, 'basement_garbage_room_rating')) {
//       return false
//     }

//     else if (!validateFields(this.laundry_room_equipmentInput.getInputValue(), 'laundry_room_equipment field')) {
//       return false
//     } else if (!validateFields(this.state.laundry_room_equipment_rating, 'laundry_room_equipment_rating')) {
//       return false
//     }


//     else if (!validateFields(this.laundry_room_floorInput.getInputValue(), 'laundry_room_floor field')) {
//       return false
//     } else if (!validateFields(this.state.laundry_room_floor_rating, 'laundry_room_floor_rating')) {
//       return false
//     }


//     else if (!validateFields(this.laundry_room_wallsInput.getInputValue(), 'laundry_room_walls field')) {
//       return false
//     } else if (!validateFields(this.state.laundry_room_walls_rating, 'laundry_room_walls_rating')) {
//       return false
//     }


//     else if (!validateFields(this.laundry_room_lightningInput.getInputValue(), 'laundry_room_lightning field')) {
//       return false
//     } else if (!validateFields(this.state.laundry_room_lightning_rating, 'laundry_room_lightning_rating')) {
//       return false
//     }


//     else if (!validateFields(this.boiler_fuel_supplyInput.getInputValue(), 'boiler_fuel_supply field')) {
//       return false
//     } else if (!validateFields(this.state.boiler_fuel_supply_rating, 'boiler_fuel_supply_rating')) {
//       return false
//     }


//     else if (!validateFields(this.boiler_gauge_color_coding_markingsInput.getInputValue(), 'boiler_gauge_color_coding_markings field')) {
//       return false
//     } else if (!validateFields(this.state.boiler_gauge_color_coding_markings_rating, 'boiler_gauge_color_coding_markings_rating')) {
//       return false
//     }


//     else if (!validateFields(this.boiler_nurnersInput.getInputValue(), 'boiler_nurners field')) {
//       return false
//     } else if (!validateFields(this.state.boiler_nurners_rating, 'boiler_nurners_rating')) {
//       return false
//     }

//     else if (!validateFields(this.boiler_coilsInput.getInputValue(), 'boiler_coils field')) {
//       return false
//     } else if (!validateFields(this.state.boiler_coils_rating, 'boiler_nurners_rating')) {
//       return false
//     }


//     else if (!validateFields(this.boiler_oil_tankInput.getInputValue(), 'boiler_oil_tank field')) {
//       return false
//     } else if (!validateFields(this.state.boiler_oil_tank_rating, 'boiler_oil_tank_rating')) {
//       return false
//     }


//     else if (!validateFields(this.boiler_plumbingInput.getInputValue(), 'boiler_plumbing field')) {
//       return false

//     } else if (!validateFields(this.state.boiler_plumbing_rating, 'boiler_plumbing_rating')) {
//       return false
//     }


//     else if (!validateFields(this.boiler_leaksInput.getInputValue(), 'boiler_leaks field')) {
//       return false
//     } else if (!validateFields(this.state.boiler_leaks_rating, 'boiler_leaks_rating')) {
//       return false
//     }


//     else if (!validateFields(this.boiler_overheads_returnsInput.getInputValue(), 'boiler_overheads_returns field')) {
//       return false
//     } else if (!validateFields(this.state.boiler_overheads_returns_rating, 'boiler_overheads_returns_rating')) {
//       return false
//     }


//     else if (!validateFields(this.meters_lightningInput.getInputValue(), 'meters_lightning field')) {
//       return false
//     } else if (!validateFields(this.state.meters_lightning_rating, 'meters_lightning_rating')) {
//       return false
//     }


//     else if (!validateFields(this.meters_accessibilityInput.getInputValue(), 'meters_accessibility field')) {
//       return false
//     } else if (!validateFields(this.state.meters_accessibility_rating, 'meters_accessibility_rating')) {
//       return false
//     }


//     else if (!validateFields(this.meters_up_keepingInput.getInputValue(), 'meters_up_keeping field')) {
//       return false
//     } else if (!validateFields(this.state.meters_up_keeping_rating, 'meters_up_keeping_rating')) {
//       return false
//     }


//     else if (!validateFields(this.elevator_lightningInput.getInputValue(), 'elevator_lightning field')) {
//       return false
//     } else if (!validateFields(this.state.elevator_lightning_rating, 'elevator_lightning_rating')) {
//       return false
//     }


//     else if (!validateFields(this.elevator_signalsInput.getInputValue(), 'elevator_signals field')) {
//       return false
//     } else if (!validateFields(this.state.elevator_signals_rating, 'elevator_signals_rating')) {
//       return false
//     }
//     else if (!validateFields(this.elevator_doorsInput.getInputValue(), 'elevator_doors field')) {
//       return false
//     } else if (!validateFields(this.state.elevator_doors_rating, 'elevator_doors_rating')) {
//       return false
//     }

//    else if (!validateFields(this.elevator_cab_floorInput.getInputValue(), 'elevator_cab_floor field')) {
//       return false
//     } else if (!validateFields(this.state.elevator_cab_floor_rating, 'elevator_cab_floor_rating')) {
//       return false
//     }
//    else if (!validateFields(this.elevator_cab_wallsInput.getInputValue(), 'elevator_cab_walls field')) {
//       return false
//     } else if (!validateFields(this.state.elevator_cab_walls_rating, 'elevator_cab_walls_rating')) {
//       return false
//     }

//     else if (!validateFields(this.elevator_cab_ceillingInput.getInputValue(), 'elevator_cab_ceilling field')) {
//       return false
//     } else if (!validateFields(this.state.elevator_cab_ceilling_rating, 'elevator_cab_ceilling_rating')) {
//       return false
//     }

//     else if (!validateFields(this.elevator_floor_nmber_on_doorsInput.getInputValue(), 'elevator_floor_nmber_on_doors field')) {
//       return false
//     } else if (!validateFields(this.state.elevator_floor_nmber_on_doors_rating, 'elevator_floor_nmber_on_doors_rating')) {
//       return false
//     }


//     else if (!validateFields(this.fire_equipment_sprinklersInput.getInputValue(), 'fire_equipment_sprinklers field')) {
//       return false
//     } else if (!validateFields(this.state.fire_equipment_sprinklers_rating, 'fire_equipment_sprinklers_rating')) {
//       return false
//     }


//     else if (!validateFields(this.fire_equipment_extinguishersInput.getInputValue(), 'fire_equipment_extinguishers field')) {
//       return false
//     } else if (!validateFields(this.state.fire_equipment_extinguishers_rating, 'fire_equipment_extinguishers_ratings')) {
//       return false
//     }


//     else if (!validateFields(this.fire_equipment_alarm_systemInput.getInputValue(), 'fire_equipment_alarm_system field')) {
//       return false
//     } else if (!validateFields(this.state.fire_equipment_alarm_system_rating, 'fire_equipment_alarm_system_rating')) {
//       return false
//     }


//     else if (!validateFields(this.vacant_units_apt1Input.getInputValue(), 'vacant_units_apt1 field')) {
//       return false
//     } else if (!validateFields(this.state.vacant_units_apt1_rating, 'vacant_units_apt1_rating')) {
//       return false
//     }


//     else if (!validateFields(this.vacant_units_apt2Input.getInputValue(), 'vacant_units_apt2 field')) {
//       return false
//     } else if (!validateFields(this.state.vacant_units_apt2_rating, 'vacant_units_apt2_rating')) {
//       return false
//     }

//     else if (!validateFields(this.inventoryInput.getInputValue(), 'inventory field')) {
//       return false
//     } else if (!validateFields(this.state.inventory_rating, 'inventory_rating')) {
//       return false
//     }
//     else if (!validateFields(this.problemInput.getInputValue(), 'problem field')) {
//       return false
//     } 
//     else if (!validateFields(this.notesInput.getInputValue(), 'notes field')) {
//       return false
//     }

//     else {
//       console.log('Validation Complete')
//       this.createPDF();
//       return true
//     }
//   }

//   test=()=> {
//     var that = this;
//     async function externalStoragePermission() {
//     try {
//       const granted = await PermissionsAndroid.request(
//         PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
//         {
//           title: 'External Storage Write Permission',
//           message:'App needs access to Storage data.',
//         }
//       );
//       if (granted === PermissionsAndroid.RESULTS.GRANTED) {
//         that.testPDF();
//       } else {
//         alert('WRITE_EXTERNAL_STORAGE permission denied');
//       }
//     } catch (err) {
//       Alert.alert('Write permission err', err);
//       console.warn(err);
//     }
//    }

//     if (Platform.OS === 'android') {
//       externalStoragePermission();
//     } else {
//       this.testPDF();
//     }
//   }

//   async testPDF() {

//     console.log('yess----------')
//     let r = Math.random().toString(36).substring(7);
//     let name = 'building_' + r.toString()
//     let options = {
//         html: '<table><tr><th>Firstname</th><th>Lastname</th></tr></table>',
//         fileName: name,
//         directory: 'Documents',
//     };
//     let file = await RNHTMLtoPDF.convert(options)
//     Platform.OS == 'android' ? this.saveData(file.filePath)
//     : this.saveDataIOS(file.filePath)
//     //alert(file.filePath);
// }

//  MyText=(textLable)=>{
// return(
// <View>
// <Text style={styles.MyText}>
//     {textLable}
//   </Text>
// </View>
// )
// }
// MyInputText=(OnTextChange,textValue)=>{
//   return(
  
//     <TextInput
//       style={styles.MyInputText} 
//       ref={input => { this.textInput = input }}
//       value={textValue}
//      // onEndEditing={() => this.endEditing()}
//       maxLength={40}
//        keyboardType={Platform.OS === 'ios' ? 'ascii-capable' : 'visible-password'}
//       onChangeText={OnTextChange}
     
//       />
//   )
// }
// MyDropDown=(selectedValue,onValueChange)=>{
//   return( 
//   <View style={styles.pickerstyle}>
//     <Picker
//       style={{width: widthhh*70/100}}
//       iosHeader="Ratings"
//       placeholder="Select your rating"
//       placeholderStyle={{ marginRight:0 }}
//       mode="dropdown"
//       iosIcon={<FontAwesome name="caret-down" style={{ paddingLeft: hp('12%') }} />}
//       selectedValue={selectedValue}
//       itemStyle={{ paddingLeft: 10, paddingRight: hp('0%')}}
//       onValueChange={onValueChange}
//     >
//       {
//         this.state.items.map(items => {
//            return (<Picker.Item style={{flex:1}} label={items.label} value={items.value} key={items.label} />)
//         })
//       }
//     </Picker>
//   </View>
//   )
// }

// handleScroll(e){
//  // console.log('----------')
//   // Keyboard.dismiss()
// } 

// render() {
//   const config = {
//     velocityThreshold: 0.3,
//     directionalOffsetThreshold: 80
//   };
//     return (
//       <View style={styles.container}>
//         <ImageBackground style={styles.imagebackgroundstyle} source={background}>
//            <TouchableWithoutFeedback 
//             // onPressOut={()=> Keyboard.dismiss()}
//             // onPressIn={()=> Keyboard.dismiss()}
//             // onPress={()=> Keyboard.dismiss()}
//              >
       
//             <KeyboardAwareScrollView 
//             innerRef={ref => { this.scroll = ref; }} 
//             // ref={ref => { this.scroll = ref; }} 
//            // ref={ref => scroll = ref }
//             extraHeight={90}
//             enableAutomaticScroll={Platform.OS === 'ios'}
//             enableOnAndroid={true}
//             contentContainerStyle={{flexGrow:1}} > 
         
//          {/* <ScrollView> */}

       
//               <Image source={logo} style={styles.logostyle} />
//               <Text style={{ alignSelf: 'center', fontSize: hp('3%'), fontFamily: 'Sina-Bold' }}>Building Inspection Form</Text>

//               {this.MyText("Building")}
           
//                     <CustomTextInput
//                           inputRef={ref => this.building = ref}
//                           ref={ref => this.buildingInput = ref}
//                           style={{width:'90%'}}
//                     />

//               {/* {this.MyInputText((text) => this.setState({ building: text }),this.state.building)}
//              */}
            
//               {this.MyText("Week of (yyyy-mm-dd)")}
//                 <CustomTextInput
                         
//                           inputRef={ref => this.week_of = ref}
//                           ref={ref => this.week_ofInput = ref}
//                           style={{width:'90%'}}
//                     />
//               {/* {this.MyInputText((text) => this.setState({ week_of: text }),this.state.week_of)}
//              */}
//               {this.MyText("Manager")}
//                <CustomTextInput
                         
//                           inputRef={ref => this.manager = ref}
//                           ref={ref => this.managerInput = ref}
//                           style={{width:'90%'}}
//                     />
//               {/* {this.MyInputText((text) => this.setState({ manager: text }),this.state.manager)} */}
             
//               {this.MyText("Supt")}
//               <CustomTextInput
                         
//                           inputRef={ref => this.supt = ref}
//                           ref={ref => this.suptInput = ref}
//                           style={{width:'90%'}}
//                     />
//               {/* {this.MyInputText((text) => this.setState({ supt: text }),this.state.supt)} */}
//               {this.MyText("Choose Rating")}
//               {this.MyDropDown(this.state.supt_rating,(value) => this.setState({ supt_rating: value }))}
            
//               <Text style={{ alignSelf: 'center', fontSize: hp('2.5%'), fontWeight: 'bold', marginTop: hp('3%') }}>AREAS</Text>
//               <Text style={{ alignSelf: 'center', fontSize: hp('2.2%'), fontWeight: 'bold', marginTop: hp('1%') }}>Exterior Conditions</Text>

//               {this.MyText("Sidewalks Steps")}
//               {/* {this.MyInputText((text) => this.setState({ exterior_cond_sidewalk_steps: text }),this.state.exterior_cond_sidewalk_steps)} */}
//               <CustomTextInput
                         
//                           inputRef={ref => this.exterior_cond_sidewalk_steps = ref}
//                           ref={ref => this.exterior_cond_sidewalk_stepsInput = ref}
//                           style={{width:'90%'}}
//                     />
//               {this.MyText("Choose Rating")}
//               {this.MyDropDown(this.state.exterior_cond_sidewalk_steps_rating,(value) => this.setState({ exterior_cond_sidewalk_steps_rating: value }))}
//               {this.MyText("Cans / Cover")}
//               {/* {this.MyInputText((text) => this.setState({ exterior_cond_cans_cover: text }),this.state.exterior_cond_cans_cover)} */}
//               <CustomTextInput
                         
//                           inputRef={ref => this.exterior_cond_cans_cover = ref}
//                           ref={ref => this.exterior_cond_cans_coverInput = ref}
//                           style={{width:'90%'}}
//                     />
//               {this.MyText("Choose Rating")}
//               {this.MyDropDown(this.state.exterior_cond_cans_cover_rating,(value) => this.setState({ exterior_cond_cans_cover_rating: value }))}
//               {this.MyText("Door(s)")}
//               {/* {this.MyInputText((text) => this.setState({ exterior_cond_doors: text }),this.state.exterior_cond_doors)} */}
//               <CustomTextInput
                         
//                           inputRef={ref => this.exterior_cond_doors = ref}
//                           ref={ref => this.exterior_cond_doorsInput = ref}
//                           style={{width:'90%'}}
//                     />
//               {this.MyText("Choose Rating")}
//               {this.MyDropDown(this.state.exterior_cond_doors_rating,(value) => this.setState({ exterior_cond_doors_rating: value }))}
//               {this.MyText("Courtyard (Paved Area)")}
//               {/* {this.MyInputText((text) => this.setState({ exterior_cond_courtyard: text }),this.state.exterior_cond_courtyard)} */}
//               <CustomTextInput
                         
//                           inputRef={ref => this.exterior_cond_courtyard = ref}
//                           ref={ref => this.exterior_cond_courtyardInput = ref}
//                           style={{width:'90%'}}
//                     />
//               {this.MyText("Choose Rating")}
//               {this.MyDropDown(this.state.exterior_cond_courtyard_rating,(value) => this.setState({ exterior_cond_courtyard_rating: value }))}
//               {this.MyText("Yard Drain(s)")}
//               {/* {this.MyInputText((text) => this.setState({ exterior_cond_yard_drain: text }),this.state.exterior_cond_yard_drain)} */}
//               <CustomTextInput
                         
//                           inputRef={ref => this.exterior_cond_yard_drain = ref}
//                           ref={ref => this.exterior_cond_yard_drainInput = ref}
//                           style={{width:'90%'}}
//                     />
//               {this.MyText("Choose Rating")}
//               {this.MyDropDown(this.state.exterior_cond_yard_drain_rating,(value) => this.setState({ exterior_cond_yard_drain_rating: value }))}
//               {this.MyText("Fountain Walls")}
//               {/* {this.MyInputText((text) => this.setState({ exterior_cond_fountain_walls: text }),this.state.exterior_cond_fountain_walls)} */}
//               <CustomTextInput
                         
//                           inputRef={ref => this.exterior_cond_fountain_walls = ref}
//                           ref={ref => this.exterior_cond_fountain_wallsInput = ref}
//                           style={{width:'90%'}}
//                     />
//               {this.MyText("Choose Rating")}
//               {this.MyDropDown(this.state.exterior_cond_fountain_walls_rating,(value) => this.setState({ exterior_cond_fountain_walls_rating: value }))}
//               {this.MyText("Light(s) at Entry")}
//               {/* {this.MyInputText((text) => this.setState({ exterior_cond_lights_at_entry: text }),this.state.exterior_cond_lights_at_entry)} */}
//               <CustomTextInput
                         
//                           inputRef={ref => this.exterior_cond_lights_at_entry = ref}
//                           ref={ref => this.exterior_cond_lights_at_entryInput = ref}
//                           style={{width:'90%'}}
//                     />
//               {this.MyText("Choose Rating")}
//               {this.MyDropDown(this.state.exterior_cond_lights_at_entry_rating,(value) => this.setState({ exterior_cond_lights_at_entry_rating: value }))}
//               {this.MyText("Light(s) at Yard")}
//               {/* {this.MyInputText((text) => this.setState({ exterior_cond_lights_at_yard: text }),this.state.exterior_cond_lights_at_yard)} */}
//               <CustomTextInput
                         
//                           inputRef={ref => this.exterior_cond_lights_at_yard = ref}
//                           ref={ref => this.exterior_cond_lights_at_yardInput = ref}
//                           style={{width:'90%'}}
//                     />
//               {this.MyText("Choose Rating")}
//               {this.MyDropDown(this.state.exterior_cond_lights_at_yard_rating,(value) => this.setState({ exterior_cond_lights_at_yard_rating: value }))}
//               <Text style={styles.labelstyle}>Entrance</Text>
//               {this.MyText("Entrance Door")}
//               {/* {this.MyInputText((text) => this.setState({ entrance_entrance_door: text }),this.state
//               .entrance_entrance_door)} */}
//                  <CustomTextInput
                         
//                           inputRef={ref => this.entrance_entrance_door = ref}
//                           ref={ref => this.entrance_entrance_doorInput = ref}
//                           style={{width:'90%'}}
//                     />
//               {this.MyText("Choose Rating")}
//               {this.MyDropDown(this.state.entrance_entrance_door_rating,(value) => this.setState({ entrance_entrance_door_rating: value }))}
//               {this.MyText("Sidelight(s)")}
//               {/* {this.MyInputText((text) => this.setState({ entrance_sidelight: text }),this.state.entrance_sidelight)} */}
//               <CustomTextInput
                         
//                           inputRef={ref => this.entrance_sidelight = ref}
//                           ref={ref => this.entrance_sidelightInput = ref}
//                           style={{width:'90%'}}
//                     />
//               {this.MyText("Choose Rating")}
//               {this.MyDropDown(this.state.entrance_sidelight_rating,(value) => this.setState({ entrance_sidelight_rating: value }))}
//               {this.MyText("Intercom")}
//               {/* {this.MyInputText((text) => this.setState({ entrance_intercom: text }),this.state.entrance_intercom)} */}
//               <CustomTextInput
                         
//                           inputRef={ref => this.entrance_intercom = ref}
//                           ref={ref => this.entrance_intercomInput = ref}
//                           style={{width:'90%'}}
//                     />
//               {this.MyText("Choose Rating")}
//               {this.MyDropDown(this.state.entrance_intercom_rating,(value) => this.setState({ entrance_intercom_rating: value }))}
//               {this.MyText("Mailboxes")}
//               {/* {this.MyInputText((text) => this.setState({ entrance_mailboxes: text }),this.state.entrance_mailboxes)} */}
//               <CustomTextInput
                         
//                           inputRef={ref => this.entrance_mailboxes = ref}
//                           ref={ref => this.entrance_mailboxesInput = ref}
//                           style={{width:'90%'}}
//                     />
//               {this.MyText("Choose Rating")}
//               {this.MyDropDown(this.state.entrance_mailboxes_rating,(value) => this.setState({ entrance_mailboxes_rating: value }))}
//               {this.MyText("Floor")}
//               {/* {this.MyInputText((text) => this.setState({ entrance_floor: text }),this.state.entrance_floor)} */}
//               <CustomTextInput
                         
//                           inputRef={ref => this.entrance_floor = ref}
//                           ref={ref => this.entrance_floorInput = ref}
//                           style={{width:'90%'}}
//                     />
//               {this.MyText("Choose Rating")}
//               {this.MyDropDown(this.state.entrance_floor_rating,(value) => this.setState({ entrance_floor_rating: value }))}
//               {this.MyText("Walls")}
//               {/* {this.MyInputText((text) => this.setState({ entrance_walls: text }),this.state.entrance_walls)} */}
//               <CustomTextInput
                         
//                           inputRef={ref => this.entrance_walls = ref}
//                           ref={ref => this.entrance_wallsInput = ref}
//                           style={{width:'90%'}}
//                     />
//               {this.MyText("Choose Rating")}
//               {this.MyDropDown(this.state.entrance_walls_rating,(value) => this.setState({ entrance_walls_rating: value }))}
//               {this.MyText("Lightning")}
//               {/* {this.MyInputText((text) => this.setState({ entrance_lightning: text }),this.state.entrance_lightning)} */}
//               <CustomTextInput
                         
//                           inputRef={ref => this.entrance_lightning = ref}
//                           ref={ref => this.entrance_lightningInput = ref}
//                           style={{width:'90%'}}
//                     />
//               {this.MyText("Choose Rating")}
//               {this.MyDropDown(this.state.entrance_lightning_rating,(value) => this.setState({ entrance_lightning_rating: value }))}
//               <Text style={styles.labelstyle}>Stairwells</Text>
            
//               {this.MyText("Steps / Treads")}
//               {/* {this.MyInputText((text) => this.setState({ stairwells_steps_treads: text }),this.state.stairwells_steps_treads)} */}
//               <CustomTextInput
                         
//                           inputRef={ref => this.stairwells_steps_treads = ref}
//                           ref={ref => this.stairwells_steps_treadsInput = ref}
//                           style={{width:'90%'}}
//                     />
//               {this.MyText("Choose Rating")}
//               {this.MyDropDown(this.state.stairwells_steps_treads_rating,(value) => this.setState({ stairwells_steps_treads_rating: value }))}
//               {this.MyText("Walls")}
//               {/* {this.MyInputText((text) => this.setState({ stairwells_walls: text }),this.state.stairwells_walls)} */}
//               <CustomTextInput
                         
//                           inputRef={ref => this.stairwells_walls = ref}
//                           ref={ref => this.stairwells_wallsInput = ref}
//                           style={{width:'90%'}}
//                     />
//               {this.MyText("Choose Rating")}
//               {this.MyDropDown(this.state.stairwells_walls_rating,(value) => this.setState({ stairwells_walls_rating: value }))}
//               {this.MyText("Lightning")}
//               {/* {this.MyInputText((text) => this.setState({ stairwells_lightning: text }),this.state.stairwells_lightning)} */}
//               <CustomTextInput
                         
//                           inputRef={ref => this.stairwells_lightning = ref}
//                           ref={ref => this.stairwells_lightningInput = ref}
//                           style={{width:'90%'}}
//                     />
//               {this.MyText("Choose Rating")}
//               {this.MyDropDown(this.state.stairwells_lightning_rating,(value) => this.setState({ stairwells_lightning_rating: value }))}
//               {this.MyText("Emergency Lightning")}
//               {/* {this.MyInputText((text) => this.setState({ stairwells_emergency_lightning: text }),this.state.stairwells_emergency_lightning)} */}
//               <CustomTextInput
                         
//                           inputRef={ref => this.stairwells_emergency_lightning = ref}
//                           ref={ref => this.stairwells_emergency_lightningInput = ref}
//                           style={{width:'90%'}}
//                     />
//               {this.MyText("Choose Rating")}
//               {this.MyDropDown(this.state.stairwells_emergency_lightning_rating,(value) => this.setState({ stairwells_emergency_lightning_rating: value }))}
//               {this.MyText("Door(s)")}
//               {/* {this.MyInputText((text) => this.setState({ stairwells_doors: text }),this.state.stairwells_doors)} */}
//               <CustomTextInput
                         
//                           inputRef={ref => this.stairwells_doors = ref}
//                           ref={ref => this.stairwells_doorsInput = ref}
//                           style={{width:'90%'}}
//                     />
//               {this.MyText("Choose Rating")}
//               {this.MyDropDown(this.state.stairwells_doors_rating,(value) => this.setState({ stairwells_doors_rating: value }))}
//               {this.MyText("Window(s)")}
//               {/* {this.MyInputText((text) => this.setState({ stairwells_windows: text }),this.state.stairwells_windows)} */}
//               <CustomTextInput
                         
//                           inputRef={ref => this.stairwells_windows = ref}
//                           ref={ref => this.stairwells_windowsInput = ref}
//                           style={{width:'90%'}}
//                     />
//               {this.MyText("Choose Rating")}
//               {this.MyDropDown(this.state.stairwells_windows_rating,(value) => this.setState({ stairwells_windows_rating: value }))}
//               {this.MyText("Signs")}
//               {/* {this.MyInputText((text) => this.setState({ stairwells_signs: text }),this.state.stairwells_signs)} */}
//               <CustomTextInput
                         
//                           inputRef={ref => this.stairwells_signs = ref}
//                           ref={ref => this.stairwells_signsInput = ref}
//                           style={{width:'90%'}}
//                     />
//               {this.MyText("Choose Rating")}
//               {this.MyDropDown(this.state.stairwells_signs_rating,(value) => this.setState({ stairwells_signs_rating: value }))}
//                <Text style={styles.labelstyle}>Emergency Exit</Text>
//                {this.MyText("Lightning")}
//               {/* {this.MyInputText((text) => this.setState({ emergency_exit_lightning: text }),this.state.emergency_exit_lightning)} */}
//               <CustomTextInput
                         
//                           inputRef={ref => this.emergency_exit_lightning = ref}
//                           ref={ref => this.emergency_exit_lightningInput = ref}
//                           style={{width:'90%'}}
//                     />
//               {this.MyText("Choose Rating")}
//               {this.MyDropDown(this.state.emergency_exit_lightning_rating,(value) => this.setState({ emergency_exit_lightning_rating: value }))}
//               {this.MyText("Floor")}
//               {/* {this.MyInputText((text) => this.setState({ emergency_exit_floor: text }),this.state.emergency_exit_floor)} */}
//               <CustomTextInput
                         
//                           inputRef={ref => this.emergency_exit_floor = ref}
//                           ref={ref => this.emergency_exit_floorInput = ref}
//                           style={{width:'90%'}}
//                     />
//               {this.MyText("Choose Rating")}
//               {this.MyDropDown(this.state.emergency_exit_floor_rating,(value) => this.setState({ emergency_exit_floor_rating: value }))}
//               {this.MyText("Door(s)")}
//               {/* {this.MyInputText((text) => this.setState({ emergency_exit_doors: text }),this.state.emergency_exit_doors)} */}
//               <CustomTextInput
                         
//                           inputRef={ref => this.emergency_exit_doors = ref}
//                           ref={ref => this.emergency_exit_doorsInput = ref}
//                           style={{width:'90%'}}
//                     />
//               {this.MyText("Choose Rating")}
//               {this.MyDropDown(this.state.emergency_exit_doors_rating,(value) => this.setState({ emergency_exit_doors_rating: value }))}
//               {this.MyText("Signs")}
//               {/* {this.MyInputText((text) => this.setState({ emergency_exit_signs: text }),this.state.emergency_exit_signs)} */}
//               <CustomTextInput
                         
//                           inputRef={ref => this.emergency_exit_signs = ref}
//                           ref={ref => this.emergency_exit_signsInput = ref}
//                           style={{width:'90%'}}
//                     />
//               {this.MyText("Choose Rating")}
//               {this.MyDropDown(this.state.emergency_exit_signs_rating,(value) => this.setState({ emergency_exit_signs_rating: value }))}
//               <Text style={styles.labelstyle}>Roof</Text>
//               {this.MyText("Bulkhead")}
//               {/* {this.MyInputText((text) => this.setState({ roof_bulkhead: text }),this.state.roof_bulkhead)} */}
//               <CustomTextInput
//                           inputRef={ref => this.roof_bulkhead = ref}
//                           ref={ref => this.roof_bulkheadInput = ref}
//                           style={{width:'90%'}}
//                     />
//               {this.MyText("Choose Rating")}
//               {this.MyDropDown(this.state.roof_bulkhead_rating,(value) => this.setState({ roof_bulkhead_rating: value }))}
//               {this.MyText("Roof Doors")}
//               {/* {this.MyInputText((text) => this.setState({ roof_roof_doors: text }),this.state.roof_roof_doors)} */}
//               <CustomTextInput
                         
//                           inputRef={ref => this.roof_roof_doors = ref}
//                           ref={ref => this.roof_roof_doorsInput = ref}
//                           style={{width:'90%'}}
//                     />
//               {this.MyText("Choose Rating")}
//               {this.MyDropDown(this.state.roof_roof_doors_rating,(value) => this.setState({ roof_roof_doors_rating: value }))}
//               {this.MyText("Roof Surface")}
//               {/* {this.MyInputText((text) => this.setState({ roof_roof_surface: text }),this.state.roof_roof_surface)} */}
//               <CustomTextInput
                         
//                           inputRef={ref => this.roof_roof_surface = ref}
//                           ref={ref => this.roof_roof_surfaceInput = ref}
//                           style={{width:'90%'}}
//                     />
//               {this.MyText("Choose Rating")}
//               {this.MyDropDown(this.state.roof_roof_surface_rating,(value) => this.setState({ roof_roof_surface_rating: value }))}
//               {this.MyText("Gutter")}
//               {/* {this.MyInputText((text) => this.setState({ roof_gutter : text }),this.state.roof_gutter)} */}
//               <CustomTextInput
                         
//                           inputRef={ref => this.roof_gutter = ref}
//                           ref={ref => this.roof_gutterInput = ref}
//                           style={{width:'90%'}}
//                     />
//               {this.MyText("Choose Rating")}
//               {this.MyDropDown(this.state.roof_gutter_rating,(value) => this.setState({ roof_gutter_rating: value }))}
//               {this.MyText("Leader(s)")}
//               {/* {this.MyInputText((text) => this.setState({ roof__leaders : text }),this.state.roof__leaders)} */}
//               <CustomTextInput
                         
//                           inputRef={ref => this.roof__leaders = ref}
//                           ref={ref => this.roof__leadersInput = ref}
//                           style={{width:'90%'}}
//                     />
//               {this.MyText("Choose Rating")}
//               {this.MyDropDown(this.state.roof__leaders_rating,(value) => this.setState({ roof__leaders_rating: value }))}
//               {this.MyText("Skylight")}
//               {/* {this.MyInputText((text) => this.setState({ roof_skylight : text }),this.state.roof_skylight)} */}
//               <CustomTextInput
                         
//                           inputRef={ref => this.roof_skylight = ref}
//                           ref={ref => this.roof_skylightInput = ref}
//                           style={{width:'90%'}}
//                     />
//               {this.MyText("Choose Rating")}
//               {this.MyDropDown(this.state.roof_skylight_rating,(value) => this.setState({ roof_skylight_rating: value }))}
//               {this.MyText("Parapet(s)")}
//               {/* {this.MyInputText((text) => this.setState({ roof_parapet : text }),this.state.roof_parapet)} */}
//               <CustomTextInput
                         
//                           inputRef={ref => this.roof_parapet = ref}
//                           ref={ref => this.roof_parapetInput = ref}
//                           style={{width:'90%'}}
//                     />
//               {this.MyText("Choose Rating")}
//               {this.MyDropDown(this.state.roof_parapet_rating,(value) => this.setState({ roof_parapet_rating: value }))}
//               <Text style={styles.labelstyle}>Fire Escapes</Text>
//               {this.MyText("Ladders")}
//               {/* {this.MyInputText((text) => this.setState({ fire_escapes_ladders : text }),this.state.fire_escapes_ladders)} */}
//               <CustomTextInput
                         
//                           inputRef={ref => this.fire_escapes_ladders = ref}
//                           ref={ref => this.fire_escapes_laddersInput = ref}
//                           style={{width:'90%'}}
//                     />
//               {this.MyText("Choose Rating")}
//               {this.MyDropDown(this.state.fire_escapes_ladders_rating,(value) => this.setState({ fire_escapes_ladders_rating: value }))}
//               {this.MyText("Basket(s)")}
//               {/* {this.MyInputText((text) => this.setState({ fire_escapes_basket : text }),this.state.fire_escapes_basket)} */}
//               <CustomTextInput
                         
//                           inputRef={ref => this.fire_escapes_basket = ref}
//                           ref={ref => this.fire_escapes_basketInput = ref}
//                           style={{width:'90%'}}
//                     />
//               {this.MyText("Choose Rating")}
//               {this.MyDropDown(this.state.fire_escapes_basket_rating,(value) => this.setState({ fire_escapes_basket_rating: value }))}
//               {this.MyText("Railing(s)")}
//               {/* {this.MyInputText((text) => this.setState({ fire_escapes_railling : text }),this.state.fire_escapes_railling)} */}
//               <CustomTextInput
                         
//                           inputRef={ref => this.fire_escapes_railling = ref}
//                           ref={ref => this.fire_escapes_raillingInput = ref}
//                           style={{width:'90%'}}
//                     />
//               {this.MyText("Choose Rating")}
//               {this.MyDropDown(this.state.fire_escapes_railling_rating,(value) => this.setState({ fire_escapes_railling_rating: value }))}
//               {this.MyText("Ladder Shoes(s)")}
//               {/* {this.MyInputText((text) => this.setState({ fire_escapes_ladder_shoes : text }),this.state.fire_escapes_ladder_shoes)} */}
//               <CustomTextInput
                         
//                           inputRef={ref => this.fire_escapes_ladder_shoes = ref}
//                           ref={ref => this.fire_escapes_ladder_shoesInput = ref}
//                           style={{width:'90%'}}
//                     />
//               {this.MyText("Choose Rating")}
//               {this.MyDropDown(this.state.fire_escapes_ladder_shoes_rating,(value) => this.setState({ fire_escapes_ladder_shoes_rating: value }))}
//               <Text style={styles.labelstyle}>Lobbies(Floor Wise)</Text>
//               {this.MyText("Floor")}
//               {/* {this.MyInputText((text) => this.setState({ lobbies_floor : text }),this.state.lobbies_floor)} */}
//               <CustomTextInput
                         
//                           inputRef={ref => this.lobbies_floor = ref}
//                           ref={ref => this.lobbies_floorInput = ref}
//                           style={{width:'90%'}}
//                     />
//               {this.MyText("Choose Rating")}
//               {this.MyDropDown(this.state.lobbies_floor_rating,(value) => this.setState({ lobbies_floor_rating: value }))}
//               {this.MyText("Walls")}
//               {/* {this.MyInputText((text) => this.setState({ lobbies_walls : text }),this.state.lobbies_walls)} */}
//               <CustomTextInput
                         
//                           inputRef={ref => this.lobbies_walls = ref}
//                           ref={ref => this.lobbies_wallsInput = ref}
//                           style={{width:'90%'}}
//                     />
//               {this.MyText("Choose Rating")}
//               {this.MyDropDown(this.state.lobbies_walls_rating,(value) => this.setState({ lobbies_walls_rating: value }))}
//               {this.MyText("Lightning / Emergency ")}
//               {/* {this.MyInputText((text) => this.setState({ lobbies_lightning_emergency : text }),this.state.lobbies_lightning_emergency)} */}
//               <CustomTextInput
                         
//                           inputRef={ref => this.lobbies_lightning_emergency = ref}
//                           ref={ref => this.lobbies_lightning_emergencyInput = ref}
//                           style={{width:'90%'}}
//                     />
//               {this.MyText("Choose Rating")}
//               {this.MyDropDown(this.state.lobbies_lightning_emergency_rating,(value) => this.setState({ lobbies_lightning_emergency_rating: value }))}
//               {this.MyText("Window(s) / Sills")}
//               {/* {this.MyInputText((text) => this.setState({ lobbies_windows_sills : text }),this.state.lobbies_windows_sills)} */}
//               <CustomTextInput
                         
//                           inputRef={ref => this.lobbies_windows_sills = ref}
//                           ref={ref => this.lobbies_windows_sillsInput = ref}
//                           style={{width:'90%'}}
//                     />
//               {this.MyText("Choose Rating")}
//               {this.MyDropDown(this.state.lobbies_windows_sills_rating,(value) => this.setState({ lobbies_windows_sills_rating: value }))}
//               <Text style={styles.labelstyle}>Basement</Text>
//               {this.MyText("Basement Doors")}
//               {/* {this.MyInputText((text) => this.setState({ basement_doors : text }),this.state.basement_doors)} */}
//               <CustomTextInput
                         
//                           inputRef={ref => this.basement_doors = ref}
//                           ref={ref => this.basement_doorsInput = ref}
//                           style={{width:'90%'}}
//                     />
//               {this.MyText("Choose Rating")}
//               {this.MyDropDown(this.state.basement_doors_rating,(value) => this.setState({ basement_doors_rating: value }))}
//               {this.MyText("Stairs")}
//               {/* {this.MyInputText((text) => this.setState({ basement_stairs : text }),this.state.basement_stairs)} */}
//               <CustomTextInput
                         
//                           inputRef={ref => this.basement_stairs = ref}
//                           ref={ref => this.basement_stairsInput = ref}
//                           style={{width:'90%'}}
//                     />
//               {this.MyText("Choose Rating")}
//               {this.MyDropDown(this.state.basement_stairs_rating,(value) => this.setState({ basement_stairs_rating: value }))}
//               {this.MyText("Floor")}
//               {/* {this.MyInputText((text) => this.setState({ basement_floor : text }),this.state.basement_floor)} */}
//               <CustomTextInput
                         
//                           inputRef={ref => this.basement_floor = ref}
//                           ref={ref => this.basement_floorInput = ref}
//                           style={{width:'90%'}}
//                     />
//               {this.MyText("Choose Rating")}
//               {this.MyDropDown(this.state.basement_floor_rating,(value) => this.setState({ basement_floor_rating: value }))}
//               {this.MyText("Walls")}
//               {/* {this.MyInputText((text) => this.setState({ basement_walls : text }),this.state.basement_walls)} */}
//               <CustomTextInput
                         
//                           inputRef={ref => this.basement_walls = ref}
//                           ref={ref => this.basement_wallsInput = ref}
//                           style={{width:'90%'}}
//                     />
//               {this.MyText("Choose Rating")}
//               {this.MyDropDown(this.state.basement_walls_rating,(value) => this.setState({ basement_walls_rating: value }))}
//               {this.MyText("Lightning")}
//               {/* {this.MyInputText((text) => this.setState({ basement_lightning : text }),this.state.basement_lightning)} */}
//               <CustomTextInput
                         
//                           inputRef={ref => this.basement_lightning = ref}
//                           ref={ref => this.basement_lightningInput = ref}
//                           style={{width:'90%'}}
//                     />
//               {this.MyText("Choose Rating")}
//               {this.MyDropDown(this.state.basement_lightning_rating,(value) => this.setState({ basement_lightning_rating: value }))}
//               {this.MyText("Window(s)")}
//               {/* {this.MyInputText((text) => this.setState({ basement_windows : text }),this.state.basement_windows)} */}
//               <CustomTextInput
                         
//                           inputRef={ref => this.basement_windows = ref}
//                           ref={ref => this.basement_windowsInput = ref}
//                           style={{width:'90%'}}
//                     />
//               {this.MyText("Choose Rating")}
//               {this.MyDropDown(this.state.basement_windows_rating,(value) => this.setState({ basement_windows_rating: value }))}
//               {this.MyText("Garbage Room")}
//               {/* {this.MyInputText((text) => this.setState({ basement_garbage_room : text }),this.state.basement_garbage_room)} */}
//               <CustomTextInput
                         
//                           inputRef={ref => this.basement_garbage_room = ref}
//                           ref={ref => this.basement_garbage_roomInput = ref}
//                           style={{width:'90%'}}
//                     />
//               {this.MyText("Choose Rating")}
//               {this.MyDropDown(this.state.basement_garbage_room_rating,(value) => this.setState({ basement_garbage_room_rating: value }))}
//               <Text style={styles.labelstyle}>Laundry Room</Text>
//               {this.MyText("Equipment")}
//               {/* {this.MyInputText((text) => this.setState({ laundry_room_equipment : text }),this.state.laundry_room_equipment)} */}
//               <CustomTextInput
                         
//                           inputRef={ref => this.laundry_room_equipment = ref}
//                           ref={ref => this.laundry_room_equipmentInput = ref}
//                           style={{width:'90%'}}
//                     />
//               {this.MyText("Choose Rating")}
//               {this.MyDropDown(this.state.laundry_room_equipment_rating,(value) => this.setState({ laundry_room_equipment_rating: value }))}
//               {this.MyText("Floor")}
//               {/* {this.MyInputText((text) => this.setState({ laundry_room_floor : text }),this.state.laundry_room_floor)} */}
//               <CustomTextInput
                         
//                           inputRef={ref => this.laundry_room_floor = ref}
//                           ref={ref => this.laundry_room_floorInput = ref}
//                           style={{width:'90%'}}
//                     />
//               {this.MyText("Choose Rating")}
//               {this.MyDropDown(this.state.laundry_room_floor_rating,(value) => this.setState({ laundry_room_floor_rating: value }))}
//               {this.MyText("Walls")}
//               {/* {this.MyInputText((text) => this.setState({ laundry_room_walls : text }),this.state.laundry_room_walls)} */}
//               <CustomTextInput
                         
//                           inputRef={ref => this.laundry_room_walls = ref}
//                           ref={ref => this.laundry_room_wallsInput = ref}
//                           style={{width:'90%'}}
//                     />
//               {this.MyText("Choose Rating")}
//               {this.MyDropDown(this.state.laundry_room_walls_rating,(value) => this.setState({ laundry_room_walls_rating: value }))}
//               {this.MyText("Lightning")}
//               {/* {this.MyInputText((text) => this.setState({ laundry_room_lightning : text }),this.state.laundry_room_lightning)} */}
//               <CustomTextInput
                         
//                           inputRef={ref => this.laundry_room_lightning = ref}
//                           ref={ref => this.laundry_room_lightningInput = ref}
//                           style={{width:'90%'}}
//                     />
//               {this.MyText("Choose Rating")}
//               {this.MyDropDown(this.state.laundry_room_lightning_rating,(value) => this.setState({ laundry_room_lightning_rating: value }))}
//               <Text style={styles.labelstyle}>Boiler</Text>
//               {this.MyText("Fuel Supply")}
//               {/* {this.MyInputText((text) => this.setState({ boiler_fuel_supply : text }),this.state.boiler_fuel_supply)} */}
//               <CustomTextInput
                         
//                           inputRef={ref => this.boiler_fuel_supply = ref}
//                           ref={ref => this.boiler_fuel_supplyInput = ref}
//                           style={{width:'90%'}}
//                     />
//               {this.MyText("Choose Rating")}
//               {this.MyDropDown(this.state.boiler_fuel_supply_rating,(value) => this.setState({ boiler_fuel_supply_rating: value }))}
//               {this.MyText("Gauge(s) Color Coding, Markings")}
//               {/* {this.MyInputText((text) => this.setState({ boiler_gauge_color_coding_markings : text }),this.state.boiler_gauge_color_coding_markings)} */}
//               <CustomTextInput
                         
//                           inputRef={ref => this.boiler_gauge_color_coding_markings = ref}
//                           ref={ref => this.boiler_gauge_color_coding_markingsInput = ref}
//                           style={{width:'90%'}}
//                     />
//               {this.MyText("Choose Rating")}
//               {this.MyDropDown(this.state.boiler_gauge_color_coding_markings_rating,(value) => this.setState({ boiler_gauge_color_coding_markings_rating: value }))}
//               {this.MyText("Burners")}
//               {/* {this.MyInputText((text) => this.setState({ boiler_nurners : text }),this.state.boiler_nurners)} */}
//               <CustomTextInput
                         
//                           inputRef={ref => this.boiler_nurners = ref}
//                           ref={ref => this.boiler_nurnersInput = ref}
//                           style={{width:'90%'}}
//                     />
//               {this.MyText("Choose Rating")}
//               {this.MyDropDown(this.state.boiler_nurners_rating,(value) => this.setState({ boiler_nurners_rating: value }))}
//               {this.MyText("Coils")}
//               {/* {this.MyInputText((text) => this.setState({ boiler_coils : text }),this.state.boiler_coils)} */}
//               <CustomTextInput
                         
//                           inputRef={ref => this.boiler_coils = ref}
//                           ref={ref => this.boiler_coilsInput = ref}
//                           style={{width:'90%'}}
//                     />
//               {this.MyText("Choose Rating")}
//               {this.MyDropDown(this.state.boiler_coils_rating,(value) => this.setState({ boiler_coils_rating: value }))}
//               {this.MyText("Oil Tank")}
//               {/* {this.MyInputText((text) => this.setState({ boiler_oil_tank : text }),this.state.boiler_oil_tank)} */}
//               <CustomTextInput
                         
//                           inputRef={ref => this.boiler_oil_tank = ref}
//                           ref={ref => this.boiler_oil_tankInput = ref}
//                           style={{width:'90%'}}
//                     />
//               {this.MyText("Choose Rating")}
//               {this.MyDropDown(this.state.boiler_oil_tank_rating,(value) => this.setState({ boiler_oil_tank_rating: value }))}
//               {this.MyText("Plumbing")}
//               {/* {this.MyInputText((text) => this.setState({ boiler_plumbing : text }),this.state.boiler_plumbing)} */}
//               <CustomTextInput
                         
//                           inputRef={ref => this.boiler_plumbing = ref}
//                           ref={ref => this.boiler_plumbingInput = ref}
//                           style={{width:'90%'}}
//                     />
//               {this.MyText("Choose Rating")}
//               {this.MyDropDown(this.state.boiler_plumbing_rating,(value) => this.setState({ boiler_plumbing_rating: value }))}
//               {this.MyText("Leaks")}
//               {/* {this.MyInputText((text) => this.setState({ boiler_leaks : text }),this.state.boiler_leaks)} */}
//               <CustomTextInput
                         
//                           inputRef={ref => this.boiler_leaks = ref}
//                           ref={ref => this.boiler_leaksInput = ref}
//                           style={{width:'90%'}}
//                     />
//               {this.MyText("Choose Rating")}
//               {this.MyDropDown(this.state.boiler_leaks_rating,(value) => this.setState({ boiler_leaks_rating: value }))}
//               {this.MyText("Overheads / Returns")}
//               {/* {this.MyInputText((text) => this.setState({ boiler_overheads_returns : text }),this.state.boiler_overheads_returns)} */}
//               <CustomTextInput
                         
//                           inputRef={ref => this.boiler_overheads_returns = ref}
//                           ref={ref => this.boiler_overheads_returnsInput = ref}
//                           style={{width:'90%'}}
//                     />
//               {this.MyText("Choose Rating")}
//               {this.MyDropDown(this.state.boiler_overheads_returns_rating,(value) => this.setState({ boiler_overheads_returns_rating: value }))}
//               <Text style={styles.labelstyle}>Meter(Gas and Electric)</Text>
//               {this.MyText("Lightning")}
//               {/* {this.MyInputText((text) => this.setState({ meters_lightning : text }),this.state.meters_lightning)} */}
//               <CustomTextInput
                         
//                           inputRef={ref => this.meters_lightning = ref}
//                           ref={ref => this.meters_lightningInput = ref}
//                           style={{width:'90%'}}
//                     />
//               {this.MyText("Choose Rating")}
//               {this.MyDropDown(this.state.meters_lightning_rating,(value) => this.setState({ meters_lightning_rating: value }))}
//               {this.MyText("Accessibility")}
//               {/* {this.MyInputText((text) => this.setState({ meters_accessibility : text }),this.state.meters_accessibility)} */}
//               <CustomTextInput
                         
//                           inputRef={ref => this.meters_accessibility = ref}
//                           ref={ref => this.meters_accessibilityInput = ref}
//                           style={{width:'90%'}}
//                     />
//               {this.MyText("Choose Rating")}
//               {this.MyDropDown(this.state.meters_accessibility_rating,(value) => this.setState({ meters_accessibility_rating: value }))}
//               {this.MyText("Meters Up keeping")}
//               {/* {this.MyInputText((text) => this.setState({ meters_up_keeping : text }),this.state.meters_up_keeping)} */}
//               <CustomTextInput
                         
//                           inputRef={ref => this.meters_up_keeping = ref}
//                           ref={ref => this.meters_up_keepingInput = ref}
//                           style={{width:'90%'}}
//                     />
//               {this.MyText("Choose Rating")}
//               {this.MyDropDown(this.state.meters_up_keeping_rating,(value) => this.setState({ meters_up_keeping_rating: value }))}
//               <Text style={styles.labelstyle}>Elevator / Elevator Room</Text>
//               {this.MyText("Lightning")}
//               {/* {this.MyInputText((text) => this.setState({ elevator_lightning : text }),this.state.elevator_lightning)} */}
//               <CustomTextInput
                         
//                           inputRef={ref => this.elevator_lightning = ref}
//                           ref={ref => this.elevator_lightningInput = ref}
//                           style={{width:'90%'}}
//                     />
//               {this.MyText("Choose Rating")}
//               {this.MyDropDown(this.state.elevator_lightning_rating,(value) => this.setState({ elevator_lightning_rating: value }))}
//               {this.MyText("Signals")}
//               {/* {this.MyInputText((text) => this.setState({ elevator_signals : text }),this.state.elevator_signals)} */}
//               <CustomTextInput
                         
//                           inputRef={ref => this.elevator_signals = ref}
//                           ref={ref => this.elevator_signalsInput = ref}
//                           style={{width:'90%'}}
//                     />
//               {this.MyText("Choose Rating")}
//               {this.MyDropDown(this.state.elevator_signals_rating,(value) => this.setState({ elevator_signals_rating: value }))}
//               {this.MyText("Doors")}
//               {/* {this.MyInputText((text) => this.setState({ elevator_doors : text }),this.state.elevator_doors)} */}
//               <CustomTextInput
                         
//                           inputRef={ref => this.elevator_doors = ref}
//                           ref={ref => this.elevator_doorsInput = ref}
//                           style={{width:'90%'}}
//                     />
//               {this.MyText("Choose Rating")}
//               {this.MyDropDown(this.state.elevator_doors_rating,(value) => this.setState({ elevator_doors_rating: value }))}
//               {this.MyText("Cab Floor")}
//               {/* {this.MyInputText((text) => this.setState({ elevator_cab_floor : text }),this.state.elevator_cab_floor)} */}
//               <CustomTextInput
                         
//                           inputRef={ref => this.elevator_cab_floor = ref}
//                           ref={ref => this.elevator_cab_floorInput = ref}
//                           style={{width:'90%'}}
//                     />
//               {this.MyText("Choose Rating")}
//               {this.MyDropDown(this.state.elevator_cab_floor_rating,(value) => this.setState({ elevator_cab_floor_rating: value }))}
//               {this.MyText("Cab Walls")}
//               {/* {this.MyInputText((text) => this.setState({ elevator_cab_walls : text }),this.state.elevator_cab_walls)} */}
//               <CustomTextInput
                         
//                           inputRef={ref => this.elevator_cab_walls = ref}
//                           ref={ref => this.elevator_cab_wallsInput = ref}
//                           style={{width:'90%'}}
//                     />
//               {this.MyText("Choose Rating")}
//               {this.MyDropDown(this.state.elevator_cab_walls_rating,(value) => this.setState({ elevator_cab_walls_rating: value }))}
//               {this.MyText("Cab Ceiling")}
//               {/* {this.MyInputText((text) => this.setState({ elevator_cab_ceilling : text }),this.state.elevator_cab_ceilling)} */}
//               <CustomTextInput
                         
//                           inputRef={ref => this.elevator_cab_ceilling = ref}
//                           ref={ref => this.elevator_cab_ceillingInput = ref}
//                           style={{width:'90%'}}
//                     />
//               {this.MyText("Choose Rating")}
//               {this.MyDropDown(this.state.elevator_cab_ceilling_rating,(value) => this.setState({ elevator_cab_ceilling_rating: value }))}
//               {this.MyText("Floor Number on Doors")}
//               {/* {this.MyInputText((text) => this.setState({ elevator_floor_nmber_on_doors : text }),this.state.elevator_floor_nmber_on_doors)} */}
//               <CustomTextInput
                         
//                           inputRef={ref => this.elevator_floor_nmber_on_doors = ref}
//                           ref={ref => this.elevator_floor_nmber_on_doorsInput = ref}
//                           style={{width:'90%'}}
//                     />
//               {this.MyText("Choose Rating")}
//               {this.MyDropDown(this.state.elevator_floor_nmber_on_doors_rating,(value) => this.setState({ elevator_floor_nmber_on_doors_rating: value }))}
//               <Text style={styles.labelstyle}>Fire Equipment</Text>
//               {this.MyText("Sprinklers")}
//               {/* {this.MyInputText((text) => this.setState({ fire_equipment_sprinklers : text }),this.state.fire_equipment_sprinklers)} */}
//               <CustomTextInput
                         
//                           inputRef={ref => this.fire_equipment_sprinklers = ref}
//                           ref={ref => this.fire_equipment_sprinklersInput = ref}
//                           style={{width:'90%'}}
//                     />
//               {this.MyText("Choose Rating")}
//               {this.MyDropDown(this.state.fire_equipment_sprinklers_rating,(value) => this.setState({ fire_equipment_sprinklers_rating: value }))}
//               {this.MyText("Fire Extinguishers")}
//               {/* {this.MyInputText((text) => this.setState({ fire_equipment_extinguishers : text }),this.state.fire_equipment_extinguishers)} */}
//               <CustomTextInput
                         
//                           inputRef={ref => this.fire_equipment_extinguishers = ref}
//                           ref={ref => this.fire_equipment_extinguishersInput = ref}
//                           style={{width:'90%'}}
//                     />
//               {this.MyText("Choose Rating")}
//               {this.MyDropDown(this.state.fire_equipment_extinguishers_rating,(value) => this.setState({ fire_equipment_extinguishers_rating: value }))}
//               {this.MyText("Alarm System")}
//               {/* {this.MyInputText((text) => this.setState({ fire_equipment_alarm_system : text }),this.state.fire_equipment_alarm_system)} */}
//               <CustomTextInput
                         
//                           inputRef={ref => this.fire_equipment_alarm_system = ref}
//                           ref={ref => this.fire_equipment_alarm_systemInput = ref}
//                           style={{width:'90%'}}
//                     />
//               {this.MyText("Choose Rating")}
//               {this.MyDropDown(this.state.fire_equipment_alarm_system_rating,(value) => this.setState({ fire_equipment_alarm_system_rating: value }))}
//               <Text style={styles.labelstyle}>Vacant Units</Text>
//               {this.MyText("APT #")}
//               {/* {this.MyInputText((text) => this.setState({ vacant_units_apt1 : text }),this.state.vacant_units_apt1)} */}
//               <CustomTextInput
                         
//                           inputRef={ref => this.vacant_units_apt1 = ref}
//                           ref={ref => this.vacant_units_apt1Input = ref}
//                           style={{width:'90%'}}
//                     />
//               {this.MyText("Choose Rating")}
//               {this.MyDropDown(this.state.vacant_units_apt1_rating,(value) => this.setState({ vacant_units_apt1_rating: value }))}
             
//               {this.MyText("APT #")}
//               {/* {this.MyInputText((text) => this.setState({ vacant_units_apt2 : text }),this.state.vacant_units_apt2)} */}
//               <CustomTextInput
                         
//                           inputRef={ref => this.vacant_units_apt2 = ref}
//                           ref={ref => this.vacant_units_apt2Input = ref}
//                           style={{width:'90%'}}
//                     />
//               {this.MyText("Choose Rating")}
//               {this.MyDropDown(this.state.vacant_units_apt2_rating,(value) => this.setState({ vacant_units_apt2_rating: value }))}
//               {this.MyText("Inventory")}
//               {/* {this.MyInputText((text) => this.setState({ inventory : text }),this.state.inventory)} */}
//               <CustomTextInput
                         
//                           inputRef={ref => this.inventory = ref}
//                           ref={ref => this.inventoryInput = ref}
//                           style={{width:'90%'}}
//                     />
//               {this.MyText("Choose Rating")}
//               {this.MyDropDown(this.state.inventory_rating,(value) => this.setState({ inventory_rating: value }))}
             
//               {this.MyText("Problem (Specify)")}
//                       <CustomTextInput
                         
//                           inputRef={ref => this.problem = ref}
//                           ref={ref => this.problemInput = ref}
//                           style={{width:'90%'}}
//                           //multiline={true}
//                           onSubmitEditing={''}
//                     />

//               {this.MyText("Notes")}
    
//                 <CustomTextInput
//                         onBlur = {()=> {this.setState({bottomHeight:0}); }}
//                         onFocus = {  ()=> console.log(JSON.stringify(this.ref)) }
//                         inputRef={ref => this.notes = ref}
//                         ref={ref => this.notesInput = ref}
//                         style={{width:'90%'}}
//                         // multiline={true}
//                        // onSubmitEditing={''}
//                    />
          
//               <TouchableOpacity  onPress={() => this.isValidate()}
//               style={{ 
//                  justifyContent: 'center', 
//                  alignSelf: 'center', 
//                  borderRadius: 15,
//                  marginTop: 25,
//                   marginBottom: 80, width:'80%', 
//                   height: 50, backgroundColor: '#0059BF' ,elevation:10}}
//                 >
//                   <Text
//                    style={{ alignSelf: 'center', fontSize: hp('2%'), color: '#ffffff', fontWeight: 'bold' }}>Save</Text>
             
//                 </TouchableOpacity>
//                 <View style={{height:Platform.OS == 'android' ? this.state.bottomHeight : 0}}/>

//             {/* </ScrollView>     */}
//             </KeyboardAwareScrollView>
 
//                 </TouchableWithoutFeedback> 
//         </ImageBackground>
        
//         {this.state.loading &&
//           <View style={[
//             StyleSheet.absoluteFill,
//             { backgroundColor: 'rgba(0, 0, 0, 0.5)', 
//             justifyContent: 'center',
//             alignItems:'center' ,
//             height:heighttt}
//           ]}>
//             <ActivityIndicator size="large" color="#0000ff" />
//           </View>
//          }
//       </View>

//     )
//   }
// }


// const styles = StyleSheet.create({
//   MyText:{
//     marginLeft:hp('5.7%'),
//     marginTop:hp('3.8%'),
//     fontWeight:'bold',
//     fontSize:hp('2%'),
//   },
//   MyInputText:{ 
//     alignSelf: 'center',
//     marginTop:15,
//     width:widthhh * 0.8,
//     height:58,
//     borderColor:'#000000',
//     borderWidth:hp('0.3%'),
//     borderRadius:10,
//     backgroundColor:'#ffffff',
//     paddingLeft: hp('1%'), paddingRight: hp('1%')
//    },
//   container: {
//     flex: 1,
//     width: '100%',
//     height:'100%',
//   },

//   customtextinputstyle: {
//     alignSelf: 'center',
//     marginTop:15,

//   },

//   imagebackgroundstyle: { width: wp('100%'), height: hp('100%') },
//   logostyle: {
//     width: wp('40%'), height: hp('16%'), marginTop: hp('2.1%'), alignSelf: 'center'
//   },
//   labelstyle: { alignSelf: 'center', fontSize: hp('2.2%'), fontWeight: 'bold', marginTop: hp('4%') },
//   pickerstyle: { borderRadius: 10, width: wp('80%'), height: hp('7%'),justifyContent:'center',alignItems:'center',
//    marginTop: hp('1%'), alignSelf: 'center', borderColor: '#000000', borderWidth: hp('0.3%'), backgroundColor: '#ffffff'}
// });